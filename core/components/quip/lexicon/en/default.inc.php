<?php
/**
 * Quip
 *
 * Copyright 2010-11 by Shaun McCormick <shaun@modx.com>
 *
 * This file is part of Quip, a simple commenting component for MODx Revolution.
 *
 * Quip is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * Quip is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Quip; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package quip
 */
/**
 * Quip English language file
 *
 * @package quip
 * @subpackage lexicon
 */
$_lang['quip'] = 'Quip';
#$_lang['quip_desc'] = 'A simple commenting component.';
$_lang['quip_desc'] = 'Prosty składnik komentowania.';
#$_lang['quip.ago'] = ' ago';
$_lang['quip.ago'] = ' wstecz';
#$_lang['quip.ago_days'] = '[[+days]] days';
$_lang['quip.ago_days'] = '[[+dni]] dni';
#$_lang['quip.ago_hours'] = '[[+hours]] hrs';
$_lang['quip.ago_hours'] = '[[+godziny]] godz';
#$_lang['quip.ago_minutes'] = '[[+minutes]] min';
$_lang['quip.ago_minutes'] = '[[+minuty]] min';
#$_lang['quip.ago_seconds'] = '[[+seconds]] sec';
$_lang['quip.ago_seconds'] = '[[+sekundy]] sek';
#$_lang['quip.approve_selected'] = 'Approve Selected';
$_lang['quip.approve_selected'] = 'Zatwierdź wybrany';
#$_lang['quip.approved'] = 'Approved';
$_lang['quip.approved'] = 'Zatwierdzony';
#$_lang['quip.allowed_tags'] = 'Allowed tags: [[+tags]]';
$_lang['quip.allowed_tags'] = 'Tagi dozwolone: [[+tagi]]';
#$_lang['quip.anonymous'] = 'Anonymous';
$_lang['quip.anonymous'] = 'Anonimowy';
#$_lang['quip.author'] = 'Author';
$_lang['quip.author'] = 'Autor';
#$_lang['quip.back_to_threads'] = 'Back to Thread Listing';
$_lang['quip.back_to_threads'] = 'Powrót do listy wątków';
#$_lang['quip.body'] = 'Body';
$_lang['quip.body'] = 'Treść';
#$_lang['quip.bulk_actions'] = 'Bulk Actions';
$_lang['quip.bulk_actions'] = 'Akcje grupowe';
#$_lang['quip.close'] = 'Close';
$_lang['quip.close'] = 'Zamknij';
#$_lang['quip.comment'] = 'Comment';
$_lang['quip.comment'] = 'Komentuj';
#$_lang['quip.comment_add_new'] = 'Add a new comment:';
$_lang['quip.comment_add_new'] = 'Dodaj nowy komentarz:';
#$_lang['quip.comment_approve'] = 'Approve Comment';
$_lang['quip.comment_approve'] = 'Zatwierdź komentarz';
#$_lang['quip.comment_approve_selected'] = 'Approve Selected Comments';
$_lang['quip.comment_approve_selected'] = 'Zatwierdź wybrane komentarze';
#$_lang['quip.comment_approved'] = 'Comment Approved';
$_lang['quip.comment_approved'] = 'Komentarz zatwierdzony';
#$_lang['quip.comment_approved_msg'] = 'The comment by [[+name]] on [[+createdon]] has been successfully approved.';
!$_lang['quip.comment_approved_msg'] = 'Komentarz z [[+nazwą]] na [[+createdon]] został pozytywnie zatwierdzony.';
#$_lang['quip.comment_delete'] = 'Delete Comment';
$_lang['quip.comment_delete'] = 'Usuń komentarz';
#$_lang['quip.comment_delete_selected'] = 'Delete Selected Comments';
$_lang['quip.comment_delete_selected'] = 'Usuń wybrane komentarze';
#$_lang['quip.comment_deleted'] = 'Comment Deleted';
$_lang['quip.comment_deleted'] = 'Komentarz został usunięty';
#$_lang['quip.comment_deleted_msg'] = 'The comment by [[+name]] on [[+createdon]] has been successfully deleted.';
!$_lang['quip.comment_deleted_msg'] = 'Komentarz z [[+nazwą]] na [[+createdon]] został usunięty.';
#$_lang['quip.comment_delete_confirm'] = 'Are you sure you want to delete this comment?';
$_lang['quip.comment_delete_confirm'] = 'Na pewno chcesz usunąć ten komentarz?';
#$_lang['quip.comment_err_nf'] = 'Comment not found.';
$_lang['quip.comment_err_nf'] = 'Komentarz nie został znaleziony.';
#$_lang['quip.comment_err_ns'] = 'Comment not specified.';
$_lang['quip.comment_err_ns'] = 'Komentarz nie został określony.';
#$_lang['quip.comment_err_remove'] = 'An error occurred while trying to remove the comment.';
$_lang['quip.comment_err_remove'] = 'Powstał błąd podczas usuwania komentarza.';
#$_lang['quip.comment_err_save'] = 'An error occurred while trying to save the comment.';
$_lang['quip.comment_err_save'] = 'Powstał błąd podczas usuwania komentarza.';
#$_lang['quip.comment_remove'] = 'Permanently Remove Comment';
$_lang['quip.comment_remove'] = 'Usuń komentarz permanentnie';
#$_lang['quip.comment_remove_confirm'] = 'Are you sure you want to permanently remove this comment? This cannot be undone.';
$_lang['quip.comment_remove_confirm'] = 'Na pewno chcesz usunąć ten komentarz permanentnie? Tego nie można cofnąć.';
#$_lang['quip.comment_remove_selected'] = 'Remove Selected Comments';
$_lang['quip.comment_remove_selected'] = 'Usuń wybrane komentarze';
#$_lang['quip.comment_unapprove'] = 'Unapprove Comment';
$_lang['quip.comment_unapprove'] = 'Nie zatwierdzaj komentarza';
#$_lang['quip.comment_unapprove_selected'] = 'Unapprove Selected Comments';
$_lang['quip.comment_unapprove_selected'] = 'Nie zatwierdzaj wybranych komentarzy';
#$_lang['quip.comment_undelete'] = 'Undelete Comment';
$_lang['quip.comment_undelete'] = 'Nie usuwaj komentarza';
#$_lang['quip.comment_undelete_selected'] = 'Undelete Selected Comments';
$_lang['quip.comment_undelete_selected'] = 'Nie usuwaj wybranych komentarzy';
#$_lang['quip.comment_update'] = 'Update Comment';
$_lang['quip.comment_update'] = 'Aktualizuj komentarz';
#$_lang['quip.comment_view'] = 'View Comment';
$_lang['quip.comment_view'] = 'Zobacz komentarz';
$_lang['quip.comment_will_be_moderated'] = 'Your comment has been submitted, and is now awaiting moderation. You will receive an email when your comment has been approved.';
$_lang['quip.comment_will_be_moderated'] = 'Twój komentarz został zgłoszony i czeka na zatwierdzenie. Dostaniesz informację email kiedy Twój komentarz będzie zatwierdzony.';
$_lang['quip.comments'] = 'Komentarze';
#$_lang['quip.delete'] = 'Delete';
$_lang['quip.delete'] = 'Usunąć';
#$_lang['quip.delete_selected'] = 'Delete Selected';
$_lang['quip.delete_selected'] = 'Usuń wybrane';
$_lang['quip.email'] = 'Email';
$_lang['quip.email_err_ns'] = 'Podaj poprawny adres e-mail.';
#$_lang['quip.err_fraud_attempt'] = 'Error: IDs of users do not match. Fraud attempt detected.';
$_lang['quip.err_fraud_attempt'] = 'Błąd: ID użytkowników nie pasują. Wykryto niedozwolone działanie.';
#$_lang['quip.err_not_logged_in'] = 'You are not logged in and therefore are not authorized to post comments.';
$_lang['quip.err_not_logged_in'] = 'Nie jesteś zalogowany dlatego nie masz możliwości komentowania.';
#$_lang['quip.hide_deleted'] = 'Hide Deleted';
$_lang['quip.hide_deleted'] = 'Ukryj usunięte';
#$_lang['quip.intro_msg'] = 'Manage your Quip comments and threads here.';
$_lang['quip.intro_msg'] = 'Zarządzaj komentarzami i wątkami.';
$_lang['quip.ip'] = 'IP';
#$_lang['quip.latest_comments'] = 'Latest Comments';
$_lang['quip.latest_comments'] = 'Ostatnie komentarze';
#$_lang['quip.latest_comments_for'] = 'Latest Comments for';
$_lang['quip.latest_comments_for'] = 'Ostatnie komentarze do';
#$_lang['quip.latest_comments_msg'] = 'View the latest posted comments here.';
$_lang['quip.latest_comments_msg'] = 'Przeglądaj ostatnio publikowane komentarze.';
#$_lang['quip.login_to_comment'] = 'Please login to comment.';
$_lang['quip.login_to_comment'] = 'Zaloguj się aby komentować.';
$_lang['quip.message_err_ns'] = 'Uzupełnij komentarz.';
#$_lang['quip.moderated'] = 'Moderated';
$_lang['quip.moderated'] = 'Zatwierdzony';
#$_lang['quip.moderated_desc'] = 'New posts to this thread will be moderated.';
$_lang['quip.moderated_desc'] = 'Nowe posty do tego wątku zostaną moderowane.';
#$_lang['quip.moderators'] = 'Moderators';
$_lang['quip.moderators'] = 'Moderatorzy';
#$_lang['quip.moderators_desc'] = 'A comma-separated list of usernames who can moderate this thread.';
$_lang['quip.moderators_desc'] = 'Lista nazw użytkowników po przecinku, którzy mogą moderować ten wątek.';
#$_lang['quip.moderator_group'] = 'Moderator Group';
$_lang['quip.moderator_group'] = 'Grupa moderatora';
#$_lang['quip.moderator_group_desc'] = 'A User Group whose members can moderate this thread.';
$_lang['quip.moderator_group_desc'] = 'Grupa użytkowników, której członkowie mogą moderować ten wątek.';
#$_lang['quip.name'] = 'Name';
$_lang['quip.name'] = 'Nazwa';
$_lang['quip.name_err_ns'] = 'Podaj imię.';
#$_lang['quip.no_email_to_specified'] = 'Quip cannot send a spam report email because no admin email was specified.';
$_lang['quip.no_email_to_specified'] = 'Quip nie może wysłać raport spamu mailem ponieważ nie wybrano maila administratora.';
#$_lang['quip.notify_me'] = 'Notify of New Replies';
$_lang['quip.notify_me'] = 'Zawiadamiaj o nowych odpowiedziach';
#$_lang['quip.notification'] = 'Notification';
$_lang['quip.notification'] = 'Zgłoszenie';
#$_lang['quip.notification_create'] = 'Add Notification';
$_lang['quip.notification_create'] = 'Dodaj zgłoszenie';
#$_lang['quip.notification_err_nf'] = 'Notification not found!';
$_lang['quip.notification_err_nf'] = 'Nie znaleziono zgłoszenia!';
#$_lang['quip.notification_err_ns'] = 'Notification not specified!';
$_lang['quip.notification_err_ns'] = 'Nie określono zgłoszenia!';
#$_lang['quip.notification_remove'] = 'Remove Notification';
$_lang['quip.notification_remove'] = 'Usuń zgłoszenie';
#$_lang['quip.notification_remove_confirm'] = 'Are you sure you want to unsubscribe this user from this Thread?';
$_lang['quip.notification_remove_confirm'] = 'Na pewno chcesz wypisać tego użytkownika z tego wątku?';
#$_lang['quip.notifications'] = 'Notifications';
$_lang['quip.notifications'] = 'Zgłoszenia';
#$_lang['quip.notifications.intro_msg'] = 'Here is a list of all the subscribed users to this Thread.';
$_lang['quip.notifications.intro_msg'] = 'To jest lista wszystkich zasubskrybowanych użytkowników do tego wątku.';
#$_lang['quip.post'] = 'Post';
$_lang['quip.post'] = 'Posty';
#$_lang['quip.posted'] = 'Posted';
$_lang['quip.posted'] = 'Wysłane';
#$_lang['quip.postedon'] = 'Posted On';
$_lang['quip.postedon'] = 'Wysłane dnia';
#$_lang['quip.preview'] = 'Preview';
$_lang['quip.preview'] = 'Podgląd';
#$_lang['quip.recaptcha_err_load'] = 'Could not load reCaptcha service class.';
$_lang['quip.recaptcha_err_load'] = 'Nie może wyczytać reCaptcha.';
#$_lang['quip.reply'] = 'Reply';
$_lang['quip.reply'] = 'Odpowiedż';
#$_lang['quip.remove'] = 'Remove';
$_lang['quip.remove'] = 'Usuń';
#$_lang['quip.remove_selected'] = 'Remove Selected';
$_lang['quip.remove_selected'] = 'Usuń wybrany';
#$_lang['quip.report_as_spam'] = 'Report as Spam';
$_lang['quip.report_as_spam'] = 'Zgłoś jako spam';
#$_lang['quip.reported_as_spam'] = 'Reported as Spam';
$_lang['quip.reported_as_spam'] = 'Zgłoszone jako spam';
#$_lang['quip.sfs_err_load'] = 'Could not load StopForumSpam class.';
$_lang['quip.sfs_err_load'] = 'Nie może wyczytać StopForumSpam.';
#$_lang['quip.show_deleted'] = 'Show Deleted';
$_lang['quip.show_deleted'] = 'Pokaż usunięte';
#$_lang['quip.spam_blocked'] = 'Your submission was blocked by a spam filter: [[+fields]]';
$_lang['quip.spam_blocked'] = 'Twoje zgłoszenie zostało zablokowane przez filtr spamu: [[+pola]]';
/*$_lang['quip.spam_email'] = '<p>Hello,</p>
<p>A comment by [[+username]] has been reported as spam at:</p>

<p><a href="[[+url]]">[[+url]]</a></p>

<p>This is an automated email. Please do not reply directly. The
comment\'s ID is: <strong>[[+id]]</strong></p>

<p>
Thanks,<br />
<em>Quip</em>
</p>';*/

$_lang['quip.spam_email'] = '<p>Witam,</p>

<p>Komentarz [[+nazwa użytkownika]] został zgłoszony jako spam at:</p>

<p><a href="[[+url]]">[[+url]]</a></p>

<p>Ta wiadomość została wygenerowana automatycznie. Proszę nie odpisywać na ten adres. The
comment\'s ID is: <strong>[[+id]]</strong></p>

<p>
Dziękuję,<br />
<em>Quip</em>
</p>';
#$_lang['quip.spam_email_subject'] = '[Quip] Comment Spam Reported';
$_lang['quip.spam_email_subject'] = '[Quip] Komentarz spam został zgłoszony';
#$_lang['quip.spam_marked'] = ' - marked as spam.';
$_lang['quip.spam_marked'] = ' - oznaczony jako spam.';
#$_lang['quip.subscribed_on'] = 'Subscribed On';
$_lang['quip.subscribed_on'] = 'Zasubskrybowany';
#$_lang['quip.thread'] = 'Thread';
$_lang['quip.thread'] = 'Wątek';
#$_lang['quip.thread_autoclosed'] = 'This thread has been closed from taking new comments.';
$_lang['quip.thread_autoclosed'] = 'Ten wątek został zamknięty przez administratora.';
#$_lang['quip.thread_err_ns'] = 'No thread specified.';
$_lang['quip.thread_err_ns'] = 'Wątek nie został określony.';
#$_lang['quip.thread_err_remove'] = 'An error occurred while trying to remove the thread.';
$_lang['quip.thread_err_remove'] = 'Powstał błąd podczas usuwania wątku.';
#$_lang['quip.thread_err_save'] = 'An error occurred while trying to save the thread.';
$_lang['quip.thread_err_save'] = 'Powstał błąd podczas zapisywania wątku.';
#$_lang['quip.thread_manage'] = 'Manage Thread';
$_lang['quip.thread_manage'] = 'Zarządzaj wątkiem';
#$_lang['quip.thread_msg'] = 'Here you can manage all the comments for this particular thread.';
$_lang['quip.thread_msg'] = 'Tutaj możesz zarządzać wszystkimi komentarzami dotyczącymi tego wątku.';
#$_lang['quip.thread_remove'] = 'Remove Thread';
$_lang['quip.thread_remove'] = 'Usuń wątek';
#$_lang['quip.thread_remove_confirm'] = 'Are you sure you want to completely remove this thread, and all of its comments? This is irreversible.';
$_lang['quip.thread_remove_confirm'] = 'Na pewno chcesz całkowicie usunąć ten wątek i wszystkie jego komentarze? Te proces jest nieodwracalny.';
#$_lang['quip.thread_remove_selected'] = 'Remove Selected Threads';
$_lang['quip.thread_remove_selected'] = 'Usuń wybrane wątki';
#$_lang['quip.thread_remove_selected_confirm'] = 'Are you sure you want to remove these threads entirely? This is irreversible.';
$_lang['quip.thread_remove_selected_confirm'] = 'Na pewno chcesz całkowicie usunąć te wątki? Ten proces jest nieodwracalny.';
#$_lang['quip.thread_truncate'] = 'Truncate Thread';
$_lang['quip.thread_truncate'] = 'Przytnij wątek';
#$_lang['quip.thread_truncate_confirm'] = 'Are you sure you want to remove all comments in this thread?';
$_lang['quip.thread_truncate_confirm'] = 'Na pewno chcesz usunąć wszystkie komentarze w tym wątku?';
#$_lang['quip.thread_truncate_selected'] = 'Truncate Selected Threads';
$_lang['quip.thread_truncate_selected'] = 'Przytnij wybrane wątki';
#$_lang['quip.thread_truncate_selected_confirm'] = 'Are you sure you want to remove all comments in these threads?';
$_lang['quip.thread_truncate_selected_confirm'] = 'Na pewno chcesz usunąć wszystkie komentarze w tych wątkach?';
#$_lang['quip.threads'] = 'Threads';
$_lang['quip.threads'] = 'Wątki';
#$_lang['quip.unapproved'] = 'Unapproved';
$_lang['quip.unapproved'] = 'Niezatwierdzone';
#$_lang['quip.unapproved_comments'] = 'Unapproved Comments';
$_lang['quip.unapproved_comments'] = 'Niezatwierdzone komentarze';
#$_lang['quip.unapproved_comments_msg'] = 'Moderate any unapproved comments here.';
$_lang['quip.unapproved_comments_msg'] = 'Tutaj zarządzaj wszystkimi niezatwierdzonymi komentarzami.';
#$_lang['quip.unsubscribe_me'] = 'Unsubscribe Me from this Thread';
$_lang['quip.unsubscribe_me'] = 'Wypisz się z tego wątku';
#$_lang['quip.unsubscribed'] = 'You have been unsubscribed from this Thread. Thanks!';
$_lang['quip.unsubscribed'] = 'Zostałeś wypisany z tego wątku. Dziękuję!';
#$_lang['quip.username_said'] = '<strong>[[+username]]</strong> said:';
$_lang['quip.username_said'] = '<strong>[[+nazwa użytkownika]]</strong> powiedział:';
#$_lang['quip.view'] = 'View';
$_lang['quip.view'] = 'Zobacz';
#$_lang['quip.website'] = 'Website';
$_lang['quip.website'] = 'Strona';

#$_lang['area_tags'] = 'Tags';
$_lang['area_tags'] = 'Tagi';
#$_lang['setting_quip.allowed_tags'] = 'Allowed Tags';
$_lang['setting_quip.allowed_tags'] = 'Tagi dozwolone';
#$_lang['setting_quip.allowed_tags_desc'] = 'The tags allowed for users in comment posting. See <a target="_blank" href="http://php.net/strip_tags">php.net/strip_tags</a> for a list of acceptable values.';
$_lang['setting_quip.allowed_tags_desc'] = 'Tagi dozwolone dla użytkowników w komentowaniu. Zobacz <a target="_blank" href="http://php.net/strip_tags">php.net/strip_tags</a> dla listy dopuszczalnych wartości.';
#$_lang['setting_quip.emailsFrom'] = 'From Email';
$_lang['setting_quip.emailsFrom'] = 'Z adresu emaila';
#$_lang['setting_quip.emailsFrom_desc'] = 'The email address to send system emails from.';
$_lang['setting_quip.emailsFrom_desc'] = 'Adres email, z którego się wysyła emaile systemowe.';
#$_lang['setting_quip.emailsTo'] = 'To Email';
$_lang['setting_quip.emailsTo'] = 'Do adresu email';
#$_lang['setting_quip.emailsTo_desc'] = 'The email address to send system emails to.';
$_lang['setting_quip.emailsTo_desc'] = 'Adres email, do którego się wysyła emaile systemowe.';
#$_lang['setting_quip.emailsReplyTo'] = 'Reply-To Email';
$_lang['setting_quip.emailsReplyTo'] = 'Odpowiedź do email';
#$_lang['setting_quip.emailsReplyTo_desc'] = 'The email address to set the reply-to to. Defaults to emailFrom.';
$_lang['setting_quip.emailsReplyTo_desc'] = 'Adres email, do którego ustawić funkcję odpowiedź do. Domyślny do emailOd.';

#$_lang['setting_quip.emails_from_name'] = 'From Name for Emails';
$_lang['setting_quip.emails_from_name'] = 'Od nazwy do Emaili';
#$_lang['setting_quip.emails_from_name_desc'] = 'The name that will be used for the From: address in emails.';
$_lang['setting_quip.emails_from_name_desc'] = 'Nazwa, która będzie użyta do formularza: adres w emailach.';