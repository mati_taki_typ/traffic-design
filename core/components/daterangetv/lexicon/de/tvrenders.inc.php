<?php
/**
 * Daterange TV
 *
 * Copyright 2013 by Thomas Jakobi <thomas.jakobi@partout.info>
 *
 * Daterange TV is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * Daterange TV is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * DaterangeTV; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package daterangetv
 * @subpackage language
 *
 * German Language for Daterange TV
 */
$_lang['daterange'] = 'Zeitraum (von – bis)';
$_lang['daterangetv.from'] = 'Von Datum';
$_lang['daterangetv.to'] = 'Bis Datum';

$_lang['daterangetv.dateFormat'] = 'Datumsformat';
$_lang['daterangetv.dateFormatDesc'] = 'Das Format muss nach <a href="http://docs.sencha.com/ext-js/3-4/#!/api/Date-static-method-parseDate" target="_blank">Date.parseDate</a> gültig sein (Standard: Manager-Datumsformat).';

$_lang['daterangetv.dateOutputFormat'] = 'Datumsformat';
$_lang['daterangetv.dateOutputFormatDesc'] = 'Nach Tag, Monat und Jahr mit <em>|</em> separierte Liste von <a href="http://php.net/manual/de/function.strftime.php" target="_blank">strftime</a> Platzhaltern.';
$_lang['daterangetv.separatorOutput'] = 'Trennzeichen';
$_lang['daterangetv.separatorOutputDesc'] = 'Zeichenkette, die zwischen dem ersten und dem zweiten Teil des Zeitraums ausgegeben wird.';
$_lang['daterangetv.localeOutput'] = 'Locale';
$_lang['daterangetv.localeOutputDesc'] = '<a href="http://php.net/manual/de/function.setlocale.php">Locale</a> mit der der Zeitraum ausgegeben wird.';

?>