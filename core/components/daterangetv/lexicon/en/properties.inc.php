<?php
/**
 * Daterange TV
 *
 * Copyright 2013 by Thomas Jakobi <thomas.jakobi@partout.info>
 *
 * Daterange TV is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * Daterange TV is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * DaterangeTV; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package daterangetv
 * @subpackage lexicon
 *
 * Properties English Lexicon Entries for daterange
 */

$_lang['prop_daterange.value'] = 'The Value that should be formatted';
$_lang['prop_daterange.format'] = 'A between day, month and year by | separated list of strftime placeholders';
$_lang['prop_daterange.separator'] = 'String between the first and second part of the daterange';
$_lang['prop_daterange.locale'] = 'Locale the daterange is formatted with';
