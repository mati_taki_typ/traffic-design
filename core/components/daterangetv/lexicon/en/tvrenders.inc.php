<?php
/**
 * Daterange TV
 *
 * Copyright 2013 by Thomas Jakobi <thomas.jakobi@partout.info>
 *
 * Daterange TV is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * Daterange TV is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * DaterangeTV; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package daterangetv
 * @subpackage language
 *
 * English Language for Daterange TV
 */
$_lang['daterange'] = 'Date Range (From <> To)';
$_lang['daterangetv.from'] = 'From Date';
$_lang['daterangetv.to'] = 'To Date';

$_lang['daterangetv.dateFormat'] = 'Date Format';
$_lang['daterangetv.dateFormatDesc'] = 'The format must be valid according to <a href="http://docs.sencha.com/ext-js/3-4/#!/api/Date-static-method-parseDate" target="_blank">Date.parseDate</a> (defaults to manager date format).';

$_lang['daterangetv.dateOutputFormat'] = 'Date Format';
$_lang['daterangetv.dateOutputFormatDesc'] = 'A between day, month and year by <em>|</em> separated list of <a href="http://php.net/manual/en/function.strftime.php" target="_blank">strftime</a> placeholders.';
$_lang['daterangetv.separatorOutput'] = 'Separator';
$_lang['daterangetv.separatorOutputDesc'] = 'String between the first and second part of the daterange.';
$_lang['daterangetv.localeOutput'] = 'Locale';
$_lang['daterangetv.localeOutputDesc'] = '<a href="http://php.net/manual/de/function.setlocale.php">Locale</a> the daterange is formatted with.';

?>