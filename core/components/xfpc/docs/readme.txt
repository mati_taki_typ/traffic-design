--------------------
Package: xFPC
--------------------
Version: 1.1.0
First Released: November 13th, 2012
Author: Patrick Nijkamp (b03tz) <patrick@scherpontwikkeling.nl>
Author: SCHERP Ontwikkeling - http://www.scherpontwikkeling.nl
License: GNU GPLv2 (or later at your option)

RTFM for help: http://rtfm.modx.com/display/ADDON/xFPC

This component is a MODX full-page caching system. It even allows
for dynamic content to be place through an easy to use AJAX snippet.
When the package is installed it will immediately start caching
your webpages bringing your speed up to a few ms per page request. 