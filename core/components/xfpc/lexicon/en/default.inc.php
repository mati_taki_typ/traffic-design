<?php

$_lang['setting_xfpc.enabled'] = 'Enable xFPC Caching';
$_lang['setting_xfpc.exclude'] = 'Exclude URLs';
$_lang['setting_xfpc.cachelife'] = 'Cache lifetime';
$_lang['setting_xfpc.excludecss'] = 'Exclude CSS files';
$_lang['setting_xfpc.excludejs'] = 'Exclude JS files';
$_lang['setting_xfpc.combinecss'] = 'Combine CSS files (will break relative paths!)';
$_lang['setting_xfpc.combinejs'] = 'Combine JS files';
$_lang['setting_xfpc.combinejsandcss'] = 'EXPERIMENTAL: Combine JS and CSS';
$_lang['setting_xfpc.lifetimetv'] = 'Per-resource lifetime TV';
$_lang['setting_xfpc.minifycss'] = 'Minify CSS';
$_lang['setting_xfpc.minifyjs'] = 'Minify JS';

$_lang['setting_xfpc.exclude_desc'] = 'Enter a keyword to exclude from the cache (taken from the URL). Enter one keyword on each new line, when the word is present in an URL it wont be cached.';
$_lang['setting_xfpc.cachelife_desc'] = 'The global lifetime of xFPC\'s cache in seconds. 0 = Infinite';
$_lang['setting_xfpc.excludecss_desc'] = 'Comma separated list of CSS files to exclude (just the filename, not the complete path)';
$_lang['setting_xfpc.excludejs_desc'] = 'Comma separated list of JS files to exclude (just the filename, not the complete path)';
$_lang['setting_xfpc.combinecss_desc'] = 'Take all non-excluded CSS files from the document HEAD and combine them into 1 cached file (will break relative paths!)';
$_lang['setting_xfpc.combinejs_desc'] = 'Take all non-excluded JS files from the document HEAD and combine them into 1 cached file';
$_lang['setting_xfpc.combinejsandcss_desc'] = 'EXPERIMENTAL: Combine the CSS into the combined JS file. Saves you one more request.';
$_lang['setting_xfpc.lifetimetv_desc'] = 'Enter the ID of the TV that holds the per-resource-lifetime value.';
$_lang['setting_xfpc.enabled_desc'] = 'Enable xFPC Caching, 1=yes 0=no';
$_lang['setting_xfpc.minifycss_desc'] = 'Wether or not to minify your CSS';
$_lang['setting_xfpc.minifyjs_desc'] = 'Wether or not to minify your JS'; 