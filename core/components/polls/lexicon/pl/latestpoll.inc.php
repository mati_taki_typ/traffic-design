﻿<?php
/**
 * The latest poll English Poll lexicon entries
 *
 * @package polls
 * @subpackage lexion
 * @language: English
 * @author: Bert Oost
 * @translation-date: 2010-12-21
 */

#$_lang['polls.totalvotes'] = "Total votes: [[+num]]";
$_lang['polls.totalvotes'] = "Wszystkie głosy: [[+nr]]";
#$_lang['polls.vote'] = "Vote!";
$_lang['polls.vote'] = "Głosuj!";
#$_lang['polls.votedon'] = "You have already voted on [[+date]] at [[+time]]!";
$_lang['polls.votedon'] = "Już głosowałeś [[+data]] at [[+czas]]!";
#$_lang['polls.category'] = "Category: [[+name]]";
$_lang['polls.category'] = "Kategoria: [[+nazwa]]";
#$_lang['polls.votes'] = "Votes: [[+num]]";
$_lang['polls.votes'] = "Głosy: [[+nr]]";
#$_lang['polls.showresults'] = "Show Results";
$_lang['polls.showresults'] = "Pokaż wyniki";

?>