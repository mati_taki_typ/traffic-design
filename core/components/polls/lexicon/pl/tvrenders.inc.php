﻿<?php
/**
 * The TV render English Poll lexicon entries
 *
 * @package polls
 * @subpackage lexion
 * @language: English
 * @author: Bert Oost
 * @translation-date: 2012-10-03
 */

#$_lang['pollslist'] = "Polls question list";
$_lang['pollslist'] = "Lista pytań do ankiety";
#$_lang['pollscategory'] = "Category";
$_lang['pollscategory'] = "Kategorie";
#$_lang['pollscategory_desc'] = "Specify a category to list questions only from that category.";
$_lang['pollscategory_desc'] = "Sprecyzuj kategorię aby pokazać pytania tylko z tej kategorii.";

?>