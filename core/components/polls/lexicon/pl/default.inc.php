﻿<?php
/**
 * The default English Poll lexicon entries
 *
 * @package polls
 * @subpackage lexion
 * @language: English
 * @author: Bert Oost
 * @translation-date: 2011-01-20
 */

#$_lang['polls'] = "Polls";
$_lang['polls'] = "Ankiety";
#$_lang['polls.manage'] = "Manage Polls";
$_lang['polls.manage'] = "Zarządzaj ankietami";
#$_lang['polls.desc'] = "Manage your poll categories, questions and answers here.";
$_lang['polls.desc'] = "Zarządzaj kategoriami ankiet, pytania i odpowiedzi tutaj.";
#$_lang['polls.search'] = "Search...";
$_lang['polls.search'] = "Szukaj...";
#$_lang['polls.publishdate'] = "Publishdate";
$_lang['polls.publishdate'] = "Data publikacji";
#$_lang['polls.unpublishdate'] = "Unpublishdate";
$_lang['polls.unpublishdate'] = "Data cofnięcia publikacji";
#$_lang['polls.hide'] = "Hide";
$_lang['polls.hide'] = "Schowaj";
#$_lang['polls.duplicate'] = "Duplicate";
$_lang['polls.duplicate'] = "Duplikuj";

// error strings
#$_lang['polls.error.griddata'] = "No data from the grid has been found!";
$_lang['polls.error.griddata'] = "Nie znaleziono danych z modułu!";
#$_lang['polls.error.update'] = "Could not update the record, because the record was unknown!";
$_lang['polls.error.update'] = "Nie można zaktualizować rejestru, ponieważ jest nieznany!";
#$_lang['polls.error.save'] = "Failed to save the record data!";
$_lang['polls.error.save'] = "Nie udało się zapisać danych z rejestru!";

// questions part
#$_lang['polls.questions'] = "Questions";
$_lang['polls.questions'] = "Pytania";
#$_lang['polls.question'] = "Question";
$_lang['polls.question'] = "Pytanie";
$_lang['polls.questionid'] = "QID";
#$_lang['polls.question.nocategory'] = "No category";
$_lang['polls.question.nocategory'] = "Brak kategorii";
#$_lang['polls.questions.desc'] = "Here you can manage all the questions you want to be shown on your website. You're also able to hide polls from the website or publish and unpublish polls by setting dates. Edit values simple by clicking on the value.";
$_lang['polls.questions.desc'] = "Tutaj możesz zarządzać wszystkimi pytaniami, które mają się pokazać na stronie. Możesz również ukryć ankiety ze strony lub publikować i cofać publikacje ankiet poprzez ustawienie daty. Edytuj wartości poprzez kliknięcie na nie.";
#$_lang['polls.questions.create'] = "Create Question";
$_lang['polls.questions.create'] = "Stwórz pytanie";
#$_lang['polls.questions.create.error_save'] = "Failed to save the new question. Try it again!";
$_lang['polls.questions.create.error_save'] = "Zapisanie nowego pytania nie powiodło się. Spróbuj ponownie!";
#$_lang['polls.questions.votes'] = "Votes";
$_lang['polls.questions.votes'] = "Głosy";
#$_lang['polls.questions.duplicate'] = "Duplicate question and answers";
$_lang['polls.questions.duplicate'] = "Duplikuj pytanie i odpowiedzi";
#$_lang['polls.questions.duplicate_confirm'] = "Are you sure you want to duplicate the question and the answers? This would not duplicate the results stats like number of votes.";
$_lang['polls.questions.duplicate_confirm'] = "Na pewno chcesz duplikować pytanie i odpowiedzi? To nie zduplikuje statystyki wyników jak ilość głosów.";
#$_lang['polls.questions.duplicate_error'] = "Failed duplicating question and/or answers. Try it again!";
$_lang['polls.questions.duplicate_error'] = "Duplikowanie pytania i/lub odpowiedzi nie powiodło się. Spróbuj ponownie!";
#$_lang['polls.questions.update'] = "Update Question";
$_lang['polls.questions.update'] = "Uaktualnij pytanie";
#$_lang['polls.questions.remove'] = "Remove Question";
$_lang['polls.questions.remove'] = "Usuń pytanie";
#$_lang['polls.questions.remove_confirm'] = "Are you sure you want to remove this question and all the answers and stats?";
$_lang['polls.questions.remove_confirm'] = "Na pewno chcesz usunąć to pytanie i wszystkie odpowiedzi i statystyki?";
#$_lang['polls.questions.error_remove'] = "Failed to remove the question. Try it again!";
$_lang['polls.questions.error_remove'] = "Usunięcie pytania nie powiodło się. Spróbuj ponownie!";

#$_lang['polls.question.update'] = "Update question";
$_lang['polls.question.update'] = "Uaktualnij pytanie";
#$_lang['polls.question.error_update'] = "Failed to update the question. Try it again!";
$_lang['polls.question.error_update'] = "Uaktualnienie pytania nie powiodło się. Spróbuj ponownie!";
#$_lang['polls.questions.errorload'] = "Failed to load the question!";
$_lang['polls.questions.errorload'] = "Załadowanie pytania nie powiodło się!";

// categories part
#$_lang['polls.categories'] = "Categories";
$_lang['polls.categories'] = "Kategorie";
#$_lang['polls.category'] = "Category";
$_lang['polls.category'] = "Kategoria";
#$_lang['polls.category.filter'] = "Filter on Category";
$_lang['polls.category.filter'] = "Filtrowanie kategorii";
#$_lang['polls.categoryid'] = "Category ID";
$_lang['polls.categoryid'] = "ID kategorii";
#$_lang['polls.categories.desc'] = "Below a list of the categories available for the questions. You can edit a category simple by clicking on the category name and removing by clicking with your right mousebutton.";
$_lang['polls.categories.desc'] = "Poniżej lista kategorii dostępna dla tych pytań. Możesz edytować kategorię poprzez klikanie na nazwę kategorii i usunąć klikając prawym przyciskiem myszki.";
#$_lang['polls.categories.create'] = "Create Category";
$_lang['polls.categories.create'] = "Stwórz kategorię";
#$_lang['polls.categories.create.error_save'] = "Failed to save the new category. Try it again!";
$_lang['polls.categories.create.error_save'] = "Zapisanie nowej kategorii nie powiodło się. Spróbuj ponownie!";
#$_lang['polls.categories.remove'] = "Remove Category";
$_lang['polls.categories.remove'] = "Usuń kategorię";
#$_lang['polls.categories.remove_confirm'] = "Are you sure you want to remove this category and all the questions inside it?";
$_lang['polls.categories.remove_confirm'] = "Na pewno chcesz usunąć tą kategorię i wszystkie pytania w niej zawarte?";
#$_lang['polls.categories.error_remove'] = "Failed to remove the category. Try it again!";
$_lang['polls.categories.error_remove'] = "Usunięcie kategorii nie powiodło się. Spróbuj ponownie!";

// answers part
#$_lang['polls.answers'] = "Answers";
$_lang['polls.answers'] = "Odpowiedzi";
#$_lang['polls.answer'] = "Answer";
$_lang['polls.answer'] = "Odpowiedź";
#$_lang['polls.answerid'] = "Answer ID";
$_lang['polls.answerid'] = "ID odpowiedzi";
#$_lang['polls.answers.btnback'] = "Back to questions";
$_lang['polls.answers.btnback'] = "Powrót do pytań";
#$_lang['polls.answers.desc'] = "Manage here all the answers of the selected question. You can modify the answer by clicking on the text and you can change the number of sorting for the position of the answers. Note that answers with a sorting of 0 always be the last in the list.";
$_lang['polls.answers.desc'] = "Zarządzaj tu wszystkimi odpowiedziami na wybrane pytanie. Możesz zmienić odpowiedź klikając na tekst i możesz zmienić liczbę sortowania pozycji odpowiedzi. Pamiętaj, że odpowiedzi z sortowaniem 0 będą zawsze na końcu listy.";
#$_lang['polls.answers.votes'] = "Votes";
$_lang['polls.answers.votes'] = "Głosy";
#$_lang['polls.answers.percents'] = "Percents";
$_lang['polls.answers.percents'] = "Procenty";
#$_lang['polls.answers.sort'] = "Sorting";
$_lang['polls.answers.sort'] = "Sortowanie";
#$_lang['polls.answers.create'] = "Create Answer";
$_lang['polls.answers.create'] = "Stwórz odpowiedź";
#$_lang['polls.answers.create.error_save'] = "Failed to create the new answer. Try it again!";
$_lang['polls.answers.create.error_save'] = "Stworzenie nowej odpowiedzi nie powiodło się. Spróbuj ponownie!";
#$_lang['polls.answers.remove'] = "Remove Answer";
$_lang['polls.answers.remove'] = "Usuń odpowiedź";
#$_lang['polls.answers.remove_confirm'] = "Are you sure you want to remove this answer?";
$_lang['polls.answers.remove_confirm'] = "Na pewno chcesz usunąć odpowiedź?";
#$_lang['polls.answers.error_remove'] = "Failed to remove the answer. Try it again!";
$_lang['polls.answers.error_remove'] = "Usunięcie odpowiedzi nie powiodło się. Spróbuj ponownie!";

?>