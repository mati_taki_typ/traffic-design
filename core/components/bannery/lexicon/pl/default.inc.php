<?php
/**
 * Default English Lexicon Entries for BannerY
 *
 * @package bannery
 * @subpackage lexicon
 */
$_lang['bannery'] = 'BannerY';
#$_lang['bannery.desc'] = 'BannerY - manage ads';
$_lang['bannery.desc'] = 'BannerY - zarządzaj ogłoszeniami';

#$_lang['bannery.ads'] = 'Ads';
$_lang['bannery.ads'] = 'Ogłoszenia';
#$_lang['bannery.ads.name'] = 'Name';
$_lang['bannery.ads.name'] = 'Nazwa';
$_lang['bannery.ads.url'] = 'URL';
#$_lang['bannery.ads.url.description'] = 'You can specify site resource or an http address';
$_lang['bannery.ads.url.description'] = 'Możesz sprecyzować zasoby strony albo adres http';
#$_lang['bannery.ads.image'] = 'Image';
$_lang['bannery.ads.image'] = 'Obraz';
#$_lang['bannery.ads.image.new'] = 'New image';
$_lang['bannery.ads.image.new'] = 'Nowy obraz';
#$_lang['bannery.ads.image.current'] = 'Current image';
$_lang['bannery.ads.image.current'] = 'Aktualny obraz';
#$_lang['bannery.ads.active'] = 'Active';
$_lang['bannery.ads.active'] = 'Aktywny';
#$_lang['bannery.ads.clicks'] = 'Clicks';
$_lang['bannery.ads.clicks'] = 'Kliknięcia';
#$_lang['bannery.ads.new'] = 'New ad';
$_lang['bannery.ads.new'] = 'Nowe ogłoszenie';
#$_lang['bannery.ads.update'] = 'Update ad';
$_lang['bannery.ads.update'] = 'Aktualizuj ogłoszenie';
#$_lang['bannery.ads.remove'] = 'Remove ad';
$_lang['bannery.ads.remove'] = 'Usuń ogłoszenie';
#$_lang['bannery.ads.remove.confirm'] = 'Are you sure you want to remove this ad?';
$_lang['bannery.ads.remove.confirm'] = 'Czy na pewno chcesz usunąć ogłoszenie?';
#$_lang['bannery.ads.error.nf'] = 'Ad not found';
$_lang['bannery.ads.error.nf'] = 'Nie znaleziono ogłoszenia';
#$_lang['bannery.ads.description'] = 'Text of banner';
$_lang['bannery.ads.description'] = 'Tekst na banerze';
#$_lang['bannery.ads.enable'] = 'Enable ad';
$_lang['bannery.ads.enable'] = 'Stwórz ogłoszenie';
#$_lang['bannery.ads.disable'] = 'Disable ad';
$_lang['bannery.ads.disable'] = 'Wyłącz ogłoszenie ';
#$_lang['bannery.ads.add'] = 'Add ad';
$_lang['bannery.ads.add'] = 'Dodaj ogłoszenie';
#$_lang['bannery.ads.error.ns'] = 'Ad not set';
$_lang['bannery.ads.error.ns'] = 'Ogłoszenie nie jest ustawione';
#$_lang['bannery.ads.source'] = 'Media source';
$_lang['bannery.ads.source'] = 'Źródło nośników';

#$_lang['bannery.positions'] = 'Positions';
$_lang['bannery.positions'] = 'Pozycje';
#$_lang['bannery.positions.name'] = 'Name';
$_lang['bannery.positions.name'] = 'Nazwa';
#$_lang['bannery.positions.clicks'] = 'Clicks';
$_lang['bannery.positions.clicks'] = 'Kliknięcia';
#$_lang['bannery.positions.new'] = 'New position';
$_lang['bannery.positions.new'] = 'Nowa pozycja';
#$_lang['bannery.positions.update'] = 'Update position';
$_lang['bannery.positions.update'] = 'Uaktualnij pozycję';
#$_lang['bannery.positions.remove'] = 'Remove position';
$_lang['bannery.positions.remove'] = 'Usuń pozycję';
#$_lang['bannery.positions.select'] = 'Select position';
$_lang['bannery.positions.select'] = 'Wybierz pozycję';
#$_lang['bannery.positions.remove.confirm'] = 'Are you sure you want to remove this position?';
$_lang['bannery.positions.remove.confirm'] = 'Czy na pewno chcesz usunąć tą pozycję?';
#$_lang['bannery.positions.error.nf'] = 'Position not found';
$_lang['bannery.positions.error.nf'] = 'Nie znaleziono pozycji';
#$_lang['bannery.positions.error.ns'] = 'Position not set';
$_lang['bannery.positions.error.ns'] = 'Nie ustawiono pozycji';

#$_lang['bannery.adposition.idx'] = 'Index';
$_lang['bannery.adposition.idx'] = 'Indeks';
#$_lang['bannery.adposition.remove'] = 'Remove';
$_lang['bannery.adposition.remove'] = 'Usuń';
#$_lang['bannery.adposition.error.ae'] = 'This ad is already enabled in this position.';
$_lang['bannery.adposition.error.ae'] = 'To ogłoszenie zostało już stworzone w tej pozycji.';

#$_lang['bannery.stats'] = 'Statistics';
$_lang['bannery.stats'] = 'Statystyki';
#$_lang['bannery.stats.clicks'] = 'Clicks';
$_lang['bannery.stats.clicks'] = 'Kliknięcia';
#$_lang['bannery.stats.referrers'] = 'Referrers';
$_lang['bannery.stats.referrers'] = 'Referenci';
#$_lang['bannery.stats.referrer'] = 'Referrer';
$_lang['bannery.stats.referrer'] = 'Referent';
#$_lang['bannery.stats.thismonth'] = 'This month';
$_lang['bannery.stats.thismonth'] = 'Obecny miesiąc';
#$_lang['bannery.stats.lastmonth'] = 'Last month';
$_lang['bannery.stats.lastmonth'] = 'Ostatni mięsiąc';
#$_lang['bannery.stats.thisyear'] = 'This year';
$_lang['bannery.stats.thisyear'] = 'Obecny rok';
#$_lang['bannery.stats.today'] = 'Today';
$_lang['bannery.stats.today'] = 'Dzisiaj';
#$_lang['bannery.stats.overall'] = 'Overall';
$_lang['bannery.stats.overall'] = 'Ogólny';

#$_lang['bannery.error.no_positions'] = 'You must create at least one position for placement of banners.';
$_lang['bannery.error.no_positions'] = 'Musisz stworzyć przynajmniej jedną pozycję umieszczenia banerów.';