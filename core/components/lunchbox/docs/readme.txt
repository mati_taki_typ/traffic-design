Lunchbox
-----

Lunchbox is a custom resource class (CRC) designed to navigate deep folder hierarchies in an organized and paginated way that does not gobble up endless resources in the MODX manager.  It is an alternative to Grid Class Key (http://modx.com/extras/package/gridclasskey) and Collections (http://modx.com/extras/package/collections).  Although we liked these other packages, they did not meet our specific needs for navigating deep folder hierarchies.

See a screencast tutorial on Youtube: http://youtu.be/5WTm9CyJbxc

Lunchbox is released as a "pay-what-you-want" extra.  To use it, you must obtain a license key at https://craftsmancoding.com/products/downloads/lunchbox/ but in the spirit of openness and sharing, there is no fixed price for a license.

It does take time and energy to code and maintain this extra, so I ask you to contribute what you feel is fair given how useful you find the plugin and your particular economic situation.


Author: Everett Griffiths <everett@craftsmancoding.com>
Copyright 2015

Official Documentation: https://github.com/craftsmancoding/lunchbox

Bugs and Feature Requests: https://github.com/craftsmancoding/lunchbox/issues

