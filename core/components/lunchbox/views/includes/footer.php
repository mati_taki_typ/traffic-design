
				</div><!-- /lunchbox_canvas -->

				<div id="lunchbox_footer">

					<ul>
						<li class="lunchbox_nav_item"><a class="lunchbox_bug_link" href="https://github.com/craftsmancoding/lunchbox/issues/new"><?php print $this->modx->lexicon('lunchbox.menu.bug'); ?></a></li>
						<li class="lunchbox_nav_item"><a class="lunchbox_wiki_link" href="https://github.com/craftsmancoding/lunchbox/wiki"><?php print $this->modx->lexicon('lunchbox.menu.wiki'); ?></a></li>
						<li class="lunchbox_nav_item"><a class="lunchbox_support_link" href="http://craftsmancoding.com/products/support/"><?php print $this->modx->lexicon('lunchbox.menu.support'); ?></a></li>
						
					</ul>
				   
				    <div id="lunchbox_copyright">&copy; 2015 and beyond by <a href="https://craftsmancoding.com/">Craftsman Coding</a></div>
				</div>
			</div>
		</div>
	</div>
</div>