<p>Dear subscriber,</p>
<p>you successfully finished your subscription to our newsletter service!</p>
<p>We received the following credentials:</p>
<p>
    Name: [[+fullname]]<br>
    Email: [[+email]]
</p>
<p>
    <strong>Please note:</strong> Each newsletter will contain links to immediately cancel or edit your newsletter profile.
</p>
<p>
    <em>Best wishes,<br>
    Your [[++site_name]] Team</em>
</p>
