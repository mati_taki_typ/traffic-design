<p>Dear subscriber,</p>
<p>thank you for your interest in our newsletter service! To activate your subscription, please click on the following link:</p>
<p><a href="[[+confirmUrl]]">[[+confirmUrl]]</a></p>
<p>Only after finishing this step you will receive our newsletters.</p>
<p>If you did not request this message, please ignore/delete it!</p>
<p>
<em>Thank you,<br>
Your [[++site_name]] Team</em>
</p>