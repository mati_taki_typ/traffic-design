<?php

$xpdo_meta_map = array (
  'modResource' => 
  array (
    0 => 'GoodNewsResourceContainer',
    1 => 'GoodNewsResourceMailing',
  ),
  'xPDOSimpleObject' => 
  array (
    0 => 'GoodNewsMailingMeta',
    1 => 'GoodNewsSubscriberMeta',
    2 => 'GoodNewsGroup',
    3 => 'GoodNewsGroupMember',
    4 => 'GoodNewsCategory',
    5 => 'GoodNewsCategoryMember',
    6 => 'GoodNewsProcess',
  ),
);