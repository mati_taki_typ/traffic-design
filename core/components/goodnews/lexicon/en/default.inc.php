<?php
/**
 * GoodNews
 *
 * Copyright 2012 by bitego <office@bitego.com>
 *
 * GoodNews is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * GoodNews is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this software; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

/**
 * GoodNews default
 *
 * @package goodnews
 * @subpackage lexicon
 * @language en
 */

$_lang['goodnews']                                                  = 'GoodNews';
$_lang['goodnews.desc']                                             = 'An integrated group and newsletter mailing system.';
$_lang['goodnews.credits']                                          = 'Credits';
$_lang['goodnews.credits_modx_community']                           = 'Many thanks to the stunning MODx community for their strenuous help!';
$_lang['goodnews.credits_bob_ray']                                  = 'Methods to generate full URLs and convert inline image styles by Bob Ray - <a href="http://bobsguides.com">http://bobsguides.com</a>';
$_lang['goodnews.credits_icons_by']                                 = 'Icons by: FatCow Web Hosting - <a href="http://www.fatcow.com">http://www.fatcow.com</a>';
$_lang['goodnews.system_checks']                                    = 'System Checks';
$_lang['goodnews.system_check_title']                               = 'Title';
$_lang['goodnews.system_check_value']                               = 'Value (Status)';
$_lang['goodnews.multi_processing_for_sending']                     = 'Multi processing for sending mails';
$_lang['goodnews.imap_extension_available']                         = 'IMAP extension available (Bounce Handling)';
$_lang['goodnews.site_status']                                      = 'Site is published to the web';
$_lang['goodnews.ok']                                               = '<span class="gon-ok">Yes</span>';
$_lang['goodnews.nok']                                              = '<span class="gon-nok">No</span>';
$_lang['goodnews.on']                                               = 'ON';
$_lang['goodnews.off']                                              = 'OFF';
$_lang['goodnews.id']                                               = 'ID';
$_lang['goodnews.management']                                       = 'GoodNews - Group & Newsletter Mailings';
$_lang['goodnews.button_settings']                                  = 'Settings';
$_lang['goodnews.input_search_filter']                              = 'Searchfilter...';
$_lang['goodnews.button_filter_clear']                              = 'Reset';
$_lang['goodnews.msg_loading']                                      = 'Loading...';
$_lang['goodnews.msg_saving']                                       = 'Saving...';
$_lang['goodnews.msg_saving_successfull']                           = 'The changed System-Settings are saved.';
$_lang['goodnews.msg_failed']                                       = 'Failed';
$_lang['goodnews.choose_container']                                 = 'Choose Mailing Container';
$_lang['goodnews.container_switching']                              = 'Switching Mailing Container';

$_lang['goodnews.mail_plaintext_preview']                           = 'Plain-Text Mail Preview';
$_lang['goodnews.mail_subject']                                     = 'Mail Subject';
$_lang['goodnews.mail_body']                                        = 'Mail Body';

$_lang['goodnews.newsletters']                                      = 'Mailings';
$_lang['goodnews.newsletters_management_desc']                      = 'Manage your mailings from all GoodNews containers here. The grid represents status informations to your group and newsletter mail documents. To display additional informations, please click at the email icon. To get live informations during sending, please activate <strong>Auto-Refresh</strong>.';
$_lang['goodnews.newsletter_create']                                = 'Create Mailing';
$_lang['goodnews.newsletter_id']                                    = 'ID';
$_lang['goodnews.newsletter_title']                                 = 'Mailing Title';
$_lang['goodnews.newsletter_sender']                                = 'Sent By';
$_lang['goodnews.newsletter_sent_on']                               = 'Sent On';
$_lang['goodnews.newsletter_finished_on']                           = 'Finished On';
$_lang['goodnews.newsletter_categories']                            = 'Categories';
$_lang['goodnews.newsletter_recipients']                            = 'Recipients: ';
$_lang['goodnews.newsletter_recipients_sent']                       = 'Recipients / Sent';
$_lang['goodnews.newsletter_status']                                = 'Status';
$_lang['goodnews.newsletter_status_finished']                       = 'Sent';
$_lang['goodnews.newsletter_status_stopped']                        = 'Stopped';
$_lang['goodnews.newsletter_status_in_progress']                    = 'Sending ...';
$_lang['goodnews.newsletter_status_not_yet_sent']                   = 'Not sent';
$_lang['goodnews.newsletter_status_not_ready_to_send']              = 'Incomplete';
$_lang['goodnews.newsletter_status_not_published']                  = 'Not published';
$_lang['goodnews.newsletter_status_scheduled']                      = 'Scheduled';
$_lang['goodnews.newsletter_sent_scheduled']                        = 'Sent scheduled';
$_lang['goodnews.newsletter_createdby']                             = 'Created by';
$_lang['goodnews.newsletter_createdon']                             = 'Created on';
$_lang['goodnews.newsletter_publishedby']                           = 'Published by';
$_lang['goodnews.newsletter_publishedon']                           = 'Published on';
$_lang['goodnews.newsletter_scheduled']                             = 'Scheduled';
$_lang['goodnews.newsletter_soft_bounces']                          = 'Soft bounces';
$_lang['goodnews.newsletter_hard_bounces']                          = 'Hard bounces';
$_lang['goodnews.newsletter_testrecipients']                        = 'Testrecipients';
$_lang['goodnews.newsletter_no_testrecipients']                     = 'No testrecipients defined.';
$_lang['goodnews.newsletter_filter']                                = 'Mailing Filter...';
$_lang['goodnews.newsletter_filter_all']                            = 'All Mailings';
$_lang['goodnews.newsletter_filter_scheduled']                      = 'Scheduled';
$_lang['goodnews.newsletters_none']                                 = 'No mailings';
$_lang['goodnews.newsletter_prepare']                               = 'Prepare Mailing';
$_lang['goodnews.newsletter_view']                                  = 'View Mail';
$_lang['goodnews.newsletter_preview']                               = 'Preview Mail';
$_lang['goodnews.newsletter_publish']                               = 'Publish Mailing';
$_lang['goodnews.newsletter_unpublish']                             = 'Unpublish Mailing';
$_lang['goodnews.newsletter_test_send']                             = 'Test Mailing';
$_lang['goodnews.newsletter_test_send_confirm']                     = 'Send test-mailing now?';
$_lang['goodnews.newsletter_sending_test']                          = 'Sending test-mailing';
$_lang['goodnews.newsletter_finished_sending_test']                 = 'Finished sending test-mailing.';
$_lang['goodnews.newsletter_start_sending']                         = 'Start Sending Mailing';
$_lang['goodnews.newsletter_start_sending_confirm']                 = 'Send mailing now?';
$_lang['goodnews.newsletter_sending']                               = 'Sending mailing';
$_lang['goodnews.newsletter_stop_sending']                          = 'Stop Sending Mailing';
$_lang['goodnews.newsletter_stop_sending_confirm']                  = 'Stop sending mailing now? Sending can be continued at any time.';
$_lang['goodnews.newsletter_continue_sending']                      = 'Continue Sending Mailing';
$_lang['goodnews.newsletter_continue_sending_confirm']              = 'Continue sending mailing now?';
$_lang['goodnews.newsletter_remove']                                = 'Remove Mailing';
$_lang['goodnews.newsletter_undelete']                              = 'Undelete Mailing';
$_lang['goodnews.newsletter_update']                                = 'Edit Mailing';
$_lang['goodnews.newsletter_remove_confirm']                        = 'Are you sure you want to remove this mailing?';
$_lang['goodnews.newsletter_draft']                                 = '(Draft)';
$_lang['goodnews.newsletter_cat_not_defined']                       = '(none selected)';
$_lang['goodnews.newsletter_cat_assign']                            = 'Assign mailing categories';
$_lang['goodnews.newsletter_cat_assign_desc']                       = 'Accordingly to this assignment the recipients are assorted.';
$_lang['goodnews.newsletter_sending_process_enabled']               = 'Sending Process: ENABLED';
$_lang['goodnews.newsletter_sending_process_disabled']              = 'Sending Process: DISABLED';
$_lang['goodnews.newsletter_grid_autorefresh']                      = 'Auto-Refresh: ';
$_lang['goodnews.newsletter_send_log_view']                         = 'View Send Log';
$_lang['goodnews.newsletter_send_log_window_title']                 = 'Send Log for Mailing ID: ';
$_lang['goodnews.newsletter_send_log_close_button']                 = 'Close';
$_lang['goodnews.newsletter_err_save_already_sending']              = 'Changes can not be saved, because sending of this mailing has already been startet.';

$_lang['goodnews.groups']                                           = 'Groups';
$_lang['goodnews.groups_management_desc']                           = 'Manage your subscriber groups here. Assigning a GoodNews group to a MODx user-group (<span class="gon-modx-group-assigned">blue marker</span>), makes it possible to send mailings to all members of this MODx user-group. If you assign a MODx user-group, this GoodNews group can <strong>not</strong> be used to manage subscribers.';
$_lang['goodnews.group_create']                                     = 'Create Group';
$_lang['goodnews.modx_groups_edit']                                 = 'Edit MODx User Groups';
$_lang['goodnews.group_name']                                       = 'Group Name';
$_lang['goodnews.group_description']                                = 'Description';
$_lang['goodnews.group_membercount']                                = 'Subscribers';
$_lang['goodnews.modx_usergroup']                                   = 'MODx User-Group';
$_lang['goodnews.group_belongs_to_modx_usergroup']                  = 'Assigned to MODx User Group';
$_lang['goodnews.choose_modx_user_group']                           = '(Choose MODx user group)';
$_lang['goodnews.groups_none']                                      = 'No groups';
$_lang['goodnews.group_remove']                                     = 'Remove group';
$_lang['goodnews.group_update']                                     = 'Edit group';
$_lang['goodnews.group_remove_confirm']                             = 'Are you sure you want to remove this group?<br /><br />Removing a group will also remove all it\'s related categories. In addition this will unsubscribe all members of this group and related categories.<br /><br /><strong>This can not be undone!</strong>';
$_lang['goodnews.group_err_ns_name']                                = 'Please specify a name for the group.';
$_lang['goodnews.group_err_ae']                                     = 'A group with that name already exists.';
$_lang['goodnews.group_modxgroup_err_ae']                           = 'This MODx user group is already assigned to another group.';
$_lang['goodnews.group_err_nf']                                     = 'Group not found.';
$_lang['goodnews.group_err_ns']                                     = 'Group not specified.';
$_lang['goodnews.group_err_remove']                                 = 'An error occurred while trying to remove the group.';
$_lang['goodnews.group_err_save']                                   = 'An error occurred while trying to save the group.';
                                                            
$_lang['goodnews.category']                                         = 'Category';
$_lang['goodnews.categories']                                       = 'Categories';
$_lang['goodnews.categories_management_desc']                       = 'Manage your mailing categories here.';
$_lang['goodnews.category_create']                                  = 'Create Category';
$_lang['goodnews.category_name']                                    = 'Category Name';
$_lang['goodnews.category_description']                             = 'Description';
$_lang['goodnews.category_membercount']                             = 'Subscribers';
$_lang['goodnews.category_usergroup']                               = 'Group';
$_lang['goodnews.category_belongs_to_usergroup']                    = 'Belongs to Group';
$_lang['goodnews.category_public']                                  = 'Public';
$_lang['goodnews.categories_none']                                  = 'No categories';
$_lang['goodnews.category_remove']                                  = 'Remove Category';
$_lang['goodnews.category_update']                                  = 'Edit Category';
$_lang['goodnews.category_remove_confirm']                          = 'Are you sure you want to remove this category?<br /><br />Removing a category will also unsubscribe all members of this category.<br /><br /><strong>This can not be undone!</strong>';
$_lang['goodnews.category_err_ns_name']                             = 'Please specify a name for the category.';
$_lang['goodnews.category_err_ns_group']                            = 'Please choose a group for the category.';
$_lang['goodnews.category_err_ae']                                  = 'A Category with that name already exists.';
$_lang['goodnews.category_err_nf']                                  = 'Category not found.';
$_lang['goodnews.category_err_ns']                                  = 'Category not specified.';
$_lang['goodnews.category_err_remove']                              = 'An error occurred while trying to remove the category.';
$_lang['goodnews.category_err_save']                                = 'An error occurred while trying to save the category.';
                                                            
$_lang['goodnews.subscribers']                                      = 'Subscribers';
$_lang['goodnews.subscribers_desc']                                 = 'Manage your mailing subscribers here. <span class="gon-no-subscriptions">Greyed out users</span> have no GoodNews meta-data assigned or currently no subscriptions enabled. New subscribers are created through user subscriptions, manually through MODx user management or by importing.';
$_lang['goodnews.modx_user_create']                                 = 'Create MODx User';
$_lang['goodnews.subscribers_user_group']                           = 'Subscribers Group';
$_lang['goodnews.subscribers_user_group_filter']                    = 'Filter by Group...';
$_lang['goodnews.subscribers_choose_user_group']                    = '(Choose subscribers group)';
$_lang['goodnews.subscriber_username']                              = 'Username';
$_lang['goodnews.subscriber_groups_categories']                     = 'Subscriber Groups and Categories';
$_lang['goodnews.subscriber_fullname']                              = 'Full name';
$_lang['goodnews.subscriber_email']                                 = 'Email';
$_lang['goodnews.subscriber_mailing_groups']                        = 'Mailing Groups';
$_lang['goodnews.subscriber_mailing_categories']                    = 'Mailing Categories';
$_lang['goodnews.subscriber_subscribed_on']                         = 'Subscribed on';
$_lang['goodnews.subscriber_active']                                = 'Active';
$_lang['goodnews.subscriber_testdummy']                             = 'Tester';
$_lang['goodnews.subscriber_ip']                                    = 'IP Address';
$_lang['goodnews.subscriber_ip_unknown']                            = 'unknown';
$_lang['goodnews.subscriber_ip_imported']                           = 'imported';
$_lang['goodnews.subscriber_ip_manually']                           = 'manually';
$_lang['goodnews.subscriber_soft_bounces']                          = 'SB';
$_lang['goodnews.subscriber_hard_bounces']                          = 'HB';
$_lang['goodnews.subscriber_testdummy_filter']                      = 'Test-recipient?';
$_lang['goodnews.subscriber_select_as_testdummy']                   = 'Subscriber is recipient of test-mailings and newsletters';
$_lang['goodnews.subscribers_none']                                 = 'No subscribers';
$_lang['goodnews.subscribers_no_group']                             = 'No group assigned';
$_lang['goodnews.subscriber_update']                                = 'Edit Subscriptions';
$_lang['goodnews.subscriber_remove_subscriptions']                  = 'Remove All Subscriptions';
$_lang['goodnews.subscriber_remove_meta_data']                      = 'Remove All GoodNews Meta-Data';
$_lang['goodnews.subscriber_remove_meta_data_confirm']              = 'Are you sure you want to remove all GoodNews meta-data of this user?';
$_lang['goodnews.subscriber_remove_subscriptions_confirm']          = 'Are you sure you want to remove all subscriptions of this user?';
$_lang['goodnews.user_update']                                      = 'Edit User (MODx User Table)';
                                                            
$_lang['goodnews.settings']                                         = 'GoodNews - Settings';
$_lang['goodnews.settings_save_button']                             = 'Save Settings';
$_lang['goodnews.settings_close_button']                            = 'Close';
$_lang['goodnews.settings_general_tab']                             = 'General';
$_lang['goodnews.settings_general_tab_desc']                        = 'Manage general GoodNews settings here.';
$_lang['goodnews.settings_container_tab']                           = 'Container';
$_lang['goodnews.settings_container_tab_desc']                      = 'Manage protected settings of your GoodNews resource containers here. The grid lists available GoodNews containers from the resource tree.';
$_lang['goodnews.settings_bounceparsingrules_tab']                  = 'Bounce Parsing Rules';
$_lang['goodnews.settings_bounceparsingrules_tab_desc']             = 'Manage the rules to parse Bounce-Messages here.';
$_lang['goodnews.settings_system_tab']                              = 'System';
$_lang['goodnews.settings_system_tab_desc']                         = 'Manage GoodNews system settings here.';
$_lang['goodnews.settings_about_tab']                               = 'About GoodNews';
$_lang['goodnews.settings_about_tab_desc']                          = 'GoodNews system informations.';
$_lang['goodnews.settings_test_subject_prefix']                     = 'Prefix for Test-Mailing Subjects';
$_lang['goodnews.settings_test_subject_prefix_desc']                = 'Define a text which will be prefixed to test-mailing subjects.';
$_lang['goodnews.settings_mailing_bulk_size']                       = 'Mailing-Bulk Size (1-100)';
$_lang['goodnews.settings_mailing_bulk_size_desc']                  = 'Defines how many mails are sent by one sending process.';
$_lang['goodnews.settings_mails_per_bulk']                          = ' mails per bulk';
$_lang['goodnews.settings_worker_process_limit']                    = 'Maximum Count of Sending Processes (1-10)';
$_lang['goodnews.settings_worker_process_limit_desc']               = 'Defines the maximum count of parallel sending processes to be used.';
$_lang['goodnews.settings_process_max']                             = ' processes max.';
$_lang['goodnews.settings_worker_process_active']                   = 'Sending Process activated';
$_lang['goodnews.settings_worker_process_active_desc']              = 'This setting can be used to disable the sending process. This means job scheduler like Cron can\'t trigger a new process.';
$_lang['goodnews.settings_cron_security_key']                       = 'Cron Security Key';
$_lang['goodnews.settings_cron_security_key_desc']                  = 'Please enter a random (password-like) string which will be used for Cron CLI authentification. You need to configure your Cron-job with the additional CLI parameter <strong>sid=[your cron security key]</strong>. It is strongly recommended to add a key for security reasons. Leave this field empty, if you have no possibility to configure your Cron-job with an additional parameter.';
$_lang['goodnews.settings_admin_groups']                            = 'GoodNews Administrator Groups';
$_lang['goodnews.settings_admin_groups_desc']                       = 'Comma seperated list of MODx user-groups which have access to the GoodNews settings.';
$_lang['goodnews.settings_auto_cleanup_subscriptions']              = 'Auto Cleanup Subscriptions';
$_lang['goodnews.settings_auto_cleanup_subscriptions_desc']         = 'Wether or not never activated Subscriptions, including their corresponding MODX User-Accounts, will be removed automatically. This only concerns MODX User-Accounts which have GoodNews meta-data assigned! MODX User-Accounts which belongs to MODX User-Groups will stay untouched!.';
$_lang['goodnews.settings_auto_cleanup_subscriptions_ttl']          = 'Time To Live Until Remove';
$_lang['goodnews.settings_auto_cleanup_subscriptions_ttl_desc']     = 'Number of minutes until never activated Subscriptions will be removed automatically. (Default: 360 minutes).';

$_lang['goodnews.settings_containers_none']                         = 'No GoodNews resource containers found';
$_lang['goodnews.settings_container_id']                            = 'ID';
$_lang['goodnews.settings_container_context_key']                   = 'Context';
$_lang['goodnews.settings_container_pagetitle']                     = 'Resource Container Name';
$_lang['goodnews.settings_container_editor_groups']                 = 'Container Editor Groups';
$_lang['goodnews.settings_container_editor_groups_desc']            = 'Comma seperated list of MODx user-groups which have access to this GoodNews container.';
$_lang['goodnews.settings_container_mail_from']                     = 'Sender E-Mail Address (for Bounce Messages)';
$_lang['goodnews.settings_container_mail_from_desc']                = 'The E-Mail Address used for sending mails from this container and for receiving bounce messages (Non Delivery Notifications).';
$_lang['goodnews.settings_container_mail_from_name']                = 'Sender Name';
$_lang['goodnews.settings_container_mail_from_name_desc']           = 'The Sender Name used for sending mails from this container.';
$_lang['goodnews.settings_container_mail_reply_to']                 = 'Reply-To E-Mail Address';
$_lang['goodnews.settings_container_mail_reply_to_desc']            = 'The E-Mail Address used for receiving answers to mails sent from this container.';
$_lang['goodnews.settings_container_mail_bouncehandling']           = 'Automatic Bounce Handling';
$_lang['goodnews.settings_container_mail_bouncehandling_desc']      = 'Wether to automatically analyze and handle bounce messages.';
$_lang['goodnews.settings_container_mail_service']                  = 'Account Type';
$_lang['goodnews.settings_container_mail_service_desc']             = 'The account type of the mailservice (imap or pop3).';
$_lang['goodnews.settings_container_mail_mailhost']                 = 'Mailserver Name';
$_lang['goodnews.settings_container_mail_mailhost_desc']            = 'Servername for incoming E-Mails.';
$_lang['goodnews.settings_container_mail_mailbox_username']         = 'Mailbox Username';
$_lang['goodnews.settings_container_mail_mailbox_username_desc']    = 'The username for accessing the mailbox.';
$_lang['goodnews.settings_container_mail_mailbox_password']         = 'Mailbox Password';
$_lang['goodnews.settings_container_mail_mailbox_password_desc']    = 'The password for accessing the mailbox.';
$_lang['goodnews.settings_container_mail_boxname']                  = 'Mailbox Name';
$_lang['goodnews.settings_container_mail_boxname_desc']             = 'The name of the mailbox folder (Default: INBOX).';
$_lang['goodnews.settings_container_mail_port']                     = 'Portnumber';
$_lang['goodnews.settings_container_mail_port_desc']                = 'The portnumber for the mailbox access (Default: 143).';
$_lang['goodnews.settings_container_mail_service_option']           = 'Service Option';
$_lang['goodnews.settings_container_mail_service_option_desc']      = 'The service option for the mailbox access (Default: notls).';
$_lang['goodnews.settings_container_softbounced_msg_action']        = 'Soft Bounce Messages Action';
$_lang['goodnews.settings_container_softbounced_msg_action_desc']   = 'What to do with soft bounce messages after processing?';
$_lang['goodnews.settings_container_softbounces_mailbox']           = 'Mailfolder for Soft Bounce Messages';
$_lang['goodnews.settings_container_softbounces_mailbox_desc']      = 'Name of the Mailfolder to move soft bounce messages.';
$_lang['goodnews.settings_container_max_softbounces']               = 'Max. Count of Soft Bounces';
$_lang['goodnews.settings_container_max_softbounces_desc']          = 'Maximum count of soft bounce messages for a subscriber';
$_lang['goodnews.settings_container_max_softbounces_action']        = 'Action for Max. Count of Soft Bounces';
$_lang['goodnews.settings_container_max_softbounces_action_desc']   = 'What action to perform with subscribers if maximum count of soft bounces is reached?';
$_lang['goodnews.settings_container_hardbounced_msg_action']        = 'Hard Bounce Messages Action';
$_lang['goodnews.settings_container_hardbounced_msg_action_desc']   = 'What to do with hard bounce messages after processing?';
$_lang['goodnews.settings_container_hardbounces_mailbox']           = 'Mailfolder for Hard Bounce Messages';
$_lang['goodnews.settings_container_hardbounces_mailbox_desc']      = 'Name of the Mailfolder to move hard bounce messages.';
$_lang['goodnews.settings_container_max_hardbounces']               = 'Max. Count of Hard Bounces';
$_lang['goodnews.settings_container_max_hardbounces_desc']          = 'Maximum count of hard bounce messages for a subscriber';
$_lang['goodnews.settings_container_max_hardbounces_action']        = 'Action for Max. Count of Hard Bounces';
$_lang['goodnews.settings_container_max_hardbounces_action_desc']   = 'What action to perform with subscribers if maximum count of hard bounces is reached?';
$_lang['goodnews.settings_container_notclassified_msg_action']      = 'Unclassified Messages Action';
$_lang['goodnews.settings_container_notclassified_msg_action_desc'] = 'What to do with unclassified messages?';
$_lang['goodnews.settings_container_notclassified_mailbox']         = 'Mailfolder for Unclassified Messages';
$_lang['goodnews.settings_container_notclassified_mailbox_desc']    = 'Name of the Mailfolder to move unclassified messages.';

$_lang['goodnews.settings_container_mail_pop3']                     = 'POP3';
$_lang['goodnews.settings_container_mail_imap']                     = 'IMAP';
$_lang['goodnews.settings_container_mail_none']                     = 'None';
$_lang['goodnews.settings_container_mail_tls']                      = 'TLS';
$_lang['goodnews.settings_container_mail_notls']                    = 'NO TLS';
$_lang['goodnews.settings_container_mail_ssl']                      = 'SSL';
$_lang['goodnews.settings_container_softbounced_msg_move']          = 'Move';
$_lang['goodnews.settings_container_softbounced_msg_delete']        = 'Delete';
$_lang['goodnews.settings_container_hardbounced_msg_move']          = 'Move';
$_lang['goodnews.settings_container_hardbounced_msg_delete']        = 'Delete';
$_lang['goodnews.settings_container_soft_subscriber_disable']       = 'Disable subscriber';
$_lang['goodnews.settings_container_soft_subscriber_delete']        = 'Delete subscriber';
$_lang['goodnews.settings_container_hard_subscriber_disable']       = 'Disable subscriber';
$_lang['goodnews.settings_container_hard_subscriber_delete']        = 'Delete subscriber';
$_lang['goodnews.settings_container_notclassified_msg_move']        = 'Move';
$_lang['goodnews.settings_container_notclassified_msg_delete']      = 'Delete';

$_lang['goodnews.settings_container_update']                        = 'Edit Container Settings';
$_lang['goodnews.settings_container_tab_general']                   = 'General';
$_lang['goodnews.settings_container_tab_bouncemailbox']             = 'Bounce Mailbox';
$_lang['goodnews.settings_container_tab_bouncerules']               = 'Bounce Rules';
$_lang['goodnews.settings_container_tab_notclassified_rules']       = 'Unclassified Rules';
$_lang['goodnews.settings_container_err_ns_editor_groups']          = 'Please specify at least one container editor group.';
$_lang['goodnews.settings_container_err_ns_mail_from']              = 'Please specify the sender email address (for Bounce Messages).';
$_lang['goodnews.settings_container_err_ns_mail_from_name']         = 'Please specify the sender name.';
$_lang['goodnews.settings_container_err_ns_mail_reply_to']          = 'Please specify the reply-to email address.';
$_lang['goodnews.settings_container_err_mailbox_connection_failed'] = 'Could not connect to bounce mailbox! Please check settings.';

$_lang['goodnews.import']                                           = 'GoodNews - Import';
$_lang['goodnews.import_button']                                    = 'Import';
$_lang['goodnews.import_close_button']                              = 'Close';
$_lang['goodnews.import_subscribers_tab']                           = 'Subscribers';
$_lang['goodnews.import_subscribers_tab_desc']                      = 'Import subscribers to the MODX user database. ATTENTION: Please backup your MODX database before starting the import process!';
$_lang['goodnews.import_subscribers_csvfile']                       = 'CSV File';
$_lang['goodnews.import_subscribers_csvfile_desc']                  = 'Choose a CSV file from a local file path which holds the user records to import. The file will be uploaded to the server.<br><br><strong>Required CSV format</strong>:<br>No header line! | First field = email,Second field = fullname';
$_lang['goodnews.import_subscribers_csvfile_button']                = 'Choose file...';
$_lang['goodnews.import_subscribers_batchsize']                     = 'Batch Size';
$_lang['goodnews.import_subscribers_batchsize_desc']                = 'Number of records to import. If set to 0, all records will be imported. Default is 0.';
$_lang['goodnews.import_subscribers_delimiter']                     = 'Field Delimiter Character';
$_lang['goodnews.import_subscribers_delimiter_desc']                = 'Set the field delimiter (one character only). Default is ,';
$_lang['goodnews.import_subscribers_enclosure']                     = 'Field Enclosure Character';
$_lang['goodnews.import_subscribers_enclosure_desc']                = 'The field enclosure character (one character only). Default is "';
$_lang['goodnews.import_subscribers_assign_grpcat']                 = 'Assign Mailing Groups and Categories';
$_lang['goodnews.import_subscribers_grpcat']                        = 'Mailing Groups and Categories';
$_lang['goodnews.import_subscribers_button_start']                  = 'Start import';
$_lang['goodnews.import_subscribers_status']                        = 'Import Status';
$_lang['goodnews.import_subscribers_msg_importing']                 = 'Importing...';
$_lang['goodnews.import_subscribers_msg_successfull']               = 'Import successfull';
$_lang['goodnews.import_subscribers_msg_failed']                    = 'Import failed';
$_lang['goodnews.import_subscribers_log_prep_csv_import']           = 'Preparing CSV import...';
$_lang['goodnews.import_subscribers_log_importing_csv']             = 'Importing CSV file';
$_lang['goodnews.import_subscribers_log_batchsize']                 = 'Batch size: ';
$_lang['goodnews.import_subscribers_log_imported_subscr']           = 'Imported: ';
$_lang['goodnews.import_subscribers_log_err_subscr_failed']         = 'Not imported. Saving failed: ';
$_lang['goodnews.import_subscribers_log_err_subscr_ae']             = 'Not imported. Already exists: ';
$_lang['goodnews.import_subscribers_log_err_subscr_data']           = 'Not imported. Wrong data: ';
$_lang['goodnews.import_subscribers_log_err_email_invalid']         = 'Not imported. No valid email address: ';
$_lang['goodnews.import_subscribers_log_ns_csvfile']                = 'Please select a CSV file.';
$_lang['goodnews.import_subscribers_log_wrong_filetype']            = 'Only CSV files are accepted.';
$_lang['goodnews.import_subscribers_log_no_class']                  = 'GoodNewsImportSubscribers class could not be instantiated.';
$_lang['goodnews.import_subscribers_log_ns_batchsize']              = 'Please specify a batch-size.';
$_lang['goodnews.import_subscribers_log_ns_delimiter']              = 'Please specify the field delimiter character.';
$_lang['goodnews.import_subscribers_log_ns_enclosure']              = 'Please specify the field enclosure character.';
$_lang['goodnews.import_subscribers_log_ns_grpcat']                 = 'Please select at least one GoodNews group (or multiple groups and categories).';
$_lang['goodnews.import_subscribers_log_finished']                  = 'Import finished. Successfully imported subscribers: ';
$_lang['goodnews.import_subscribers_log_failed']                    = 'Import failed. Please check error messages.';
$_lang['goodnews.import_subscribers_log_err_open_csvfile']          = 'Error opening the CSV file.';

$_lang['setting_goodnews.current_container']                        = 'Current GoodNews Container';
$_lang['setting_goodnews.current_container_desc']                   = 'ID of the current selected GoodNews container.';
$_lang['setting_goodnews.test_subject_prefix']                      = 'Prefix for Test-Mailing Subjects';
$_lang['setting_goodnews.test_subject_prefix_desc']                 = 'Define a text which will be prefixed to test-mailing subjects.';
$_lang['setting_goodnews.mailing_bulk_size']                        = 'Mailing Bulk-Size';
$_lang['setting_goodnews.mailing_bulk_size_desc']                   = 'Defines how many mails are sent by one iteration/bulk.';
$_lang['setting_goodnews.worker_process_limit']                     = 'Maximum Count of Sending Processes (1-10)';
$_lang['setting_goodnews.worker_process_limit_desc']                = 'Defines the maximum count of parallel sending processes to be used.';
$_lang['setting_goodnews.worker_process_active']                    = 'Sending Process activated';
$_lang['setting_goodnews.worker_process_active_desc']               = 'May the Cron-Job start the GoodNews sending process? This setting can be used to temporary disable the Cron trigger.';
$_lang['setting_goodnews.admin_groups']                             = 'GoodNews Administrator Groups';
$_lang['setting_goodnews.admin_groups_desc']                        = 'Comma seperated list of MODx user-groups which have access to the GoodNews settings.';
$_lang['setting_goodnews.auto_cleanup_subscriptions']               = 'Auto Cleanup Subscriptions';
$_lang['setting_goodnews.auto_cleanup_subscriptions_desc']          = 'Wether or not inactive or never activated Subscriptions, including their corresponding MODX User-Accounts, will be removed automatically. This only concerns MODX User-Accounts which have GoodNews meta-data assigned! MODX User-Accounts which belongs to MODX User-Groups stay untouched!.';
$_lang['setting_goodnews.auto_cleanup_subscriptions_ttl']           = 'Time To Live Until Remove';
$_lang['setting_goodnews.auto_cleanup_subscriptions_ttl_desc']      = 'Number of minutes until never activated Subscriptions will be removed automatically. (Default: 360 minutes).';
$_lang['setting_goodnews.cron_security_key']                        = 'Cron Security Key';
$_lang['setting_goodnews.cron_security_key_desc']                   = 'Please enter a random (password-like) string which will be used for Cron CLI authentification. You need to configure your Cron-job with the additional CLI parameter <strong>sid=[your cron security key]</strong>. It is strongly recommended to add a key for security reasons. Leave this field empty, if you have no possibility to configure your Cron-job with an additional parameter.';
$_lang['setting_goodnews.default_container_template']               = 'Default GoodNews Container Template';
$_lang['setting_goodnews.default_container_template_desc']          = 'The default Template to use when creating a new GoodNews Container';
$_lang['setting_goodnews.debug']                                    = 'GoodNews Debug Mode';
$_lang['setting_goodnews.debug_desc']                               = 'Activate / deactivate debug mode for GoodNews. If activated, additional informations will be written to the MODX error-log.';
