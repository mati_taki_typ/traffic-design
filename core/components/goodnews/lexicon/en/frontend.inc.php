<?php
/**
 * GoodNews
 *
 * Copyright 2012 by bitego <office@bitego.com>
 *
 * GoodNews is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * GoodNews is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this software; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

/**
 * GoodNews frontend
 *
 * @package goodnews
 * @subpackage lexicon
 * @language en
 */

$_lang['goodnews.user_err_save']                  = 'An error occurred while trying to save the User.';
$_lang['goodnews.user_err_nf_email']              = 'A user with this email address was not found.';
$_lang['goodnews.profile_err_nf']                 = 'Profile not found!';
$_lang['goodnews.profile_err_save']               = 'An unknown error occurred while trying to update the profile.';
$_lang['goodnews.profile_err_unsubscribe']        = 'An unknown error occurred while trying to remove subscription.';
$_lang['goodnews.profile_updated']                = 'Profile updated.';
$_lang['goodnews.profile_unsubscription']         = 'The subscription was cancelled.';

$_lang['goodnews.email_no_recipient']             = 'No recipient specified for the email.';
$_lang['goodnews.email_not_sent']                 = 'An error occurred while sending the email.';

$_lang['goodnews.validator_form_error']           = 'A form validation error occurred. Please check the values you have entered.';
$_lang['goodnews.validator_field_required']       = 'This field is required.';
$_lang['goodnews.validator_field_not_empty']      = 'This field must be empty.';
$_lang['goodnews.validator_email_taken']          = 'This email address is already in use. Please specify another email address.';
$_lang['goodnews.validator_email_invalid']        = 'Please enter a valid email address.';
$_lang['goodnews.validator_email_invalid_domain'] = 'Your email address does not have a valid domain name.';
$_lang['goodnews.validator_min_length']           = 'This field must be at least [[+length]] characters long.';
$_lang['goodnews.validator_max_length']           = 'This field cannot be more than [[+length]] characters long.';

$_lang['goodnews.spam_blocked']                   = 'Your submission was blocked by a spam filter: ';
$_lang['goodnews.spam_marked']                    = ' - marked as spam.';

$_lang['goodnews.activation_email_subject']       = 'Thanks for your interest in our newsletter service. Please activate your subscription!';
$_lang['goodnews.subscription_email_subject']     = 'Thanks for your interest in our newsletter service. Your subscription was successful!';

$_lang['goodnews.email']                          = 'Email Address';
$_lang['goodnews.fullname']                       = 'Full Name';
$_lang['goodnews.address']                        = 'Address';
$_lang['goodnews.zip']                            = 'Zip';
$_lang['goodnews.city']                           = 'City';
$_lang['goodnews.state']                          = 'State';
$_lang['goodnews.country']                        = 'Country';
$_lang['goodnews.email']                          = 'Email';
$_lang['goodnews.mobilephone']                    = 'Cellphone';
$_lang['goodnews.phone']                          = 'Phone';
$_lang['goodnews.fax']                            = 'Fax';
$_lang['goodnews.website']                        = 'Website';
$_lang['goodnews.update_profile']                 = 'Update Profile';
