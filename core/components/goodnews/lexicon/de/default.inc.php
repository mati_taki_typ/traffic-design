<?php
/**
 * GoodNews
 *
 * Copyright 2012 by bitego <office@bitego.com>
 *
 * GoodNews is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * GoodNews is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this software; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

/**
 * GoodNews default
 *
 * @package goodnews
 * @subpackage lexicon
 * @language de
 */

$_lang['goodnews']                                                  = 'GoodNews';
$_lang['goodnews.desc']                                             = 'Ein integriertes Gruppen und Newsletter Mailing System.';
$_lang['goodnews.credits']                                          = 'Credits';
$_lang['goodnews.credits_modx_community']                           = 'Herzlichen Dank der fantastischen MODx Community für ihre unermüdliche Hilfe!';
$_lang['goodnews.credits_bob_ray']                                  = 'Methoden zur Generierung vollständiger URLs und Konvertierung von inline Image Styles von Bob Ray - <a href="http://bobsguides.com">http://bobsguides.com</a>';
$_lang['goodnews.credits_icons_by']                                 = 'Icons von: FatCow Web Hosting - <a href="http://www.fatcow.com">http://www.fatcow.com</a>';
$_lang['goodnews.system_checks']                                    = 'System Tests';
$_lang['goodnews.system_check_title']                               = 'Titel';
$_lang['goodnews.system_check_value']                               = 'Wert (Status)';
$_lang['goodnews.multi_processing_for_sending']                     = 'Multi Processing für Mailversand';
$_lang['goodnews.imap_extension_available']                         = 'IMAP Erweiterung verfügbar (Bounce Handling)';
$_lang['goodnews.site_status']                                      = 'Website ist im Internet veröffentlicht';
$_lang['goodnews.ok']                                               = '<span class="gon-ok">Ja</span>';
$_lang['goodnews.nok']                                              = '<span class="gon-nok">Nein</span>';
$_lang['goodnews.on']                                               = 'EIN';
$_lang['goodnews.off']                                              = 'AUS';
$_lang['goodnews.id']                                               = 'ID';
$_lang['goodnews.management']                                       = 'GoodNews - Gruppen & Newsletter Mailings';
$_lang['goodnews.button_settings']                                  = 'Einstellungen';
$_lang['goodnews.input_search_filter']                              = 'Suchfilter...';
$_lang['goodnews.button_filter_clear']                              = 'Zurücksetzen';
$_lang['goodnews.msg_loading']                                      = 'Wird geladen...';
$_lang['goodnews.msg_saving']                                       = 'Wird gespeichert...';
$_lang['goodnews.msg_saving_successfull']                           = 'Die geänderten Systemeinstellungen wurden gespeichert.';
$_lang['goodnews.msg_failed']                                       = 'Fehlgeschlagen';
$_lang['goodnews.choose_container']                                 = 'Auswahl Mailing Container';
$_lang['goodnews.container_switching']                              = 'Mailing Container wird gewechselt';

$_lang['goodnews.mail_plaintext_preview']                           = 'Nur-Text Mail Vorschau';
$_lang['goodnews.mail_subject']                                     = 'Mail Betreff';
$_lang['goodnews.mail_body']                                        = 'Mail Inhalt';

$_lang['goodnews.newsletters']                                      = 'Mailings';
$_lang['goodnews.newsletters_management_desc']                      = 'Verwalten Sie Ihre Mailings aus allen GoodNews Containers hier. Die Tabelle bietet Statusinformationen zu Ihren Gruppen und Newsletter Mail Dokumenten. Um zusätzliche Informationen anzuzeigen, klicken Sie bitte auf das Email Icon. Um während des Versandes Live Informationen zu erhalten, aktivieren Sie bitte <strong>Auto-Refresh</strong>.';
$_lang['goodnews.newsletter_create']                                = 'Mailing erstellen';
$_lang['goodnews.newsletter_id']                                    = 'ID';
$_lang['goodnews.newsletter_title']                                 = 'Mailing Titel';
$_lang['goodnews.newsletter_sender']                                = 'Versendet von';
$_lang['goodnews.newsletter_sent_on']                               = 'Versendet am';
$_lang['goodnews.newsletter_finished_on']                           = 'Abgeschlossen am';
$_lang['goodnews.newsletter_categories']                            = 'Kategorien';
$_lang['goodnews.newsletter_recipients']                            = 'Empfänger: ';
$_lang['goodnews.newsletter_recipients_sent']                       = 'Empfänger / Gesendet';
$_lang['goodnews.newsletter_status']                                = 'Status';
$_lang['goodnews.newsletter_status_finished']                       = 'Versendet';
$_lang['goodnews.newsletter_status_stopped']                        = 'Gestoppt';
$_lang['goodnews.newsletter_status_in_progress']                    = 'Sendet ...';
$_lang['goodnews.newsletter_status_not_yet_sent']                   = 'Nicht gesendet';
$_lang['goodnews.newsletter_status_not_ready_to_send']              = 'Unvollständig';
$_lang['goodnews.newsletter_status_not_published']                  = 'Nicht veröffentlicht';
$_lang['goodnews.newsletter_status_scheduled']                      = 'Geplant';
$_lang['goodnews.newsletter_sent_scheduled']                        = 'Geplant versendet';
$_lang['goodnews.newsletter_createdby']                             = 'Erstellt von';
$_lang['goodnews.newsletter_createdon']                             = 'Erstellt am';
$_lang['goodnews.newsletter_publishedby']                           = 'Veröffentlicht von';
$_lang['goodnews.newsletter_publishedon']                           = 'Veröffentlicht am';
$_lang['goodnews.newsletter_scheduled']                             = 'Geplant';
$_lang['goodnews.newsletter_soft_bounces']                          = 'Soft Bounces';
$_lang['goodnews.newsletter_hard_bounces']                          = 'Hard Bounces';
$_lang['goodnews.newsletter_testrecipients']                        = 'Testempfänger';
$_lang['goodnews.newsletter_no_testrecipients']                     = 'Keine Testempfänger festgelegt.';
$_lang['goodnews.newsletter_filter']                                = 'Mailing Filter...';
$_lang['goodnews.newsletter_filter_all']                            = 'Alle Mailings';
$_lang['goodnews.newsletter_filter_scheduled']                      = 'Geplant';
$_lang['goodnews.newsletters_none']                                 = 'Keine Mailings';
$_lang['goodnews.newsletter_prepare']                               = 'Mailing vorbereiten';
$_lang['goodnews.newsletter_view']                                  = 'Mail betrachten';
$_lang['goodnews.newsletter_preview']                               = 'Mail Vorschau';
$_lang['goodnews.newsletter_publish']                               = 'Mailing veröffentlichen';
$_lang['goodnews.newsletter_unpublish']                             = 'Mailing zurückziehen';
$_lang['goodnews.newsletter_test_send']                             = 'Mailing Test-Versand';
$_lang['goodnews.newsletter_test_send_confirm']                     = 'Mailing Test-Versand jetzt starten?';
$_lang['goodnews.newsletter_sending_test']                          = 'Versende Test-Mailing';
$_lang['goodnews.newsletter_finished_sending_test']                 = 'Versand von Test-Mailing abgeschlossen.';
$_lang['goodnews.newsletter_start_sending']                         = 'Mailing Versand starten';
$_lang['goodnews.newsletter_start_sending_confirm']                 = 'Mailing Versand jetzt starten?';
$_lang['goodnews.newsletter_sending']                               = 'Versende Mailing';
$_lang['goodnews.newsletter_stop_sending']                          = 'Mailing Versand stoppen';
$_lang['goodnews.newsletter_stop_sending_confirm']                  = 'Mailing Versand jetzt stoppen? Der Versand kann jederzeit wieder fortgesetzt werden.';
$_lang['goodnews.newsletter_continue_sending']                      = 'Mailing Versand fortsetzen';
$_lang['goodnews.newsletter_continue_sending_confirm']              = 'Mailing Versand jetzt fortsetzen?';
$_lang['goodnews.newsletter_remove']                                = 'Mailing entfernen';
$_lang['goodnews.newsletter_undelete']                              = 'Mailing wiederherstellen';
$_lang['goodnews.newsletter_update']                                = 'Mailing bearbeiten';
$_lang['goodnews.newsletter_remove_confirm']                        = 'Sind Sie sicher, dass Sie dieses Mailing entfernen m&ouml;chten?';
$_lang['goodnews.newsletter_draft']                                 = '(Entwurf)';
$_lang['goodnews.newsletter_cat_not_defined']                       = '(keine gew&auml;hlt)';
$_lang['goodnews.newsletter_cat_assign']                            = 'Mailing Kategorien zuweisen';
$_lang['goodnews.newsletter_cat_assign_desc']                       = 'Entsprechend dieser Zuweisung erfolgt die Zusammenstellung der Empfänger.';
$_lang['goodnews.newsletter_sending_process_enabled']               = 'Sendeprozess: AKTIVIERT';
$_lang['goodnews.newsletter_sending_process_disabled']              = 'Sendeprozess: DEAKTIVIERT';
$_lang['goodnews.newsletter_grid_autorefresh']                      = 'Auto-Refresh: ';
$_lang['goodnews.newsletter_send_log_view']                         = 'Versand Protokoll öffnen';
$_lang['goodnews.newsletter_send_log_window_title']                 = 'Versand Protokoll für Mailing ID: ';
$_lang['goodnews.newsletter_send_log_close_button']                 = 'Schließen';
$_lang['goodnews.newsletter_err_save_already_sending']              = 'Änderungen können nicht gespeichert werden, da der Mailversand bereits gestartet wurde.';

$_lang['goodnews.groups']                                           = 'Gruppen';
$_lang['goodnews.groups_management_desc']                           = 'Verwalten Sie Ihre Abonnenten-Gruppen hier. Wird eine GoodNews Gruppe mit einer MODx Benutzergruppe verkn&uuml;pft (<span class="gon-modx-group-assigned">blau markiert</span>), erm&ouml;glicht dies den Versand eines Mailings an s&auml;mtliche Mitglieder dieser MODx Benutzergruppe. Bei Verkn&uuml;pfung mit einer MODx Benutzergruppe, kann diese GoodNews Gruppe <strong>nicht</strong> f&uuml;r die Verwaltung von Abonnenten verwendet werden.';
$_lang['goodnews.group_create']                                     = 'Gruppe erstellen';
$_lang['goodnews.modx_groups_edit']                                 = 'MODx Benutzer Gruppen bearbeiten';
$_lang['goodnews.group_name']                                       = 'Gruppenname';
$_lang['goodnews.group_description']                                = 'Beschreibung';
$_lang['goodnews.group_membercount']                                = 'Abonnenten';
$_lang['goodnews.modx_usergroup']                                   = 'MODx Benutzer-Gruppe';
$_lang['goodnews.group_belongs_to_modx_usergroup']                  = 'Zugeordnet zu MODx Benutzer Gruppe';
$_lang['goodnews.choose_modx_user_group']                           = '(MODx Benutzer Gruppe wählen)';
$_lang['goodnews.groups_none']                                      = 'Keine Gruppen';
$_lang['goodnews.group_remove']                                     = 'Gruppe entfernen';
$_lang['goodnews.group_update']                                     = 'Gruppe bearbeiten';
$_lang['goodnews.group_remove_confirm']                             = 'Sind Sie sicher, dass Sie diese Gruppe entfernen m&ouml;chten?<br /><br />Beim Entfernen einer Gruppe werden auch alle zugeh&ouml;rigen Kategorien entfernt. Zus&auml;tzlich werden die Abonnements aller Mitglieder dieser Gruppe und zugeh&ouml;rigen Kategorien entfernt.<br /><br /><strong>Dies kann nicht r&uuml;ckg&auml;ngig gemacht werden!</strong>';
$_lang['goodnews.group_err_ns_name']                                = 'Bitte geben Sie einen Namen f&uuml;r die Gruppe an.';
$_lang['goodnews.group_err_ae']                                     = 'Eine Gruppe mit diesem Namen existiert bereits.';
$_lang['goodnews.group_modxgroup_err_ae']                           = 'Diese MODx Benutzer Gruppe wurde bereits einer anderen Gruppe zugewiesen.';
$_lang['goodnews.group_err_nf']                                     = 'Gruppe nicht gefunden.';
$_lang['goodnews.group_err_ns']                                     = 'Gruppe nicht angegeben.';
$_lang['goodnews.group_err_remove']                                 = 'Beim Entfernen der Gruppe ist ein Fehler aufgetreten.';
$_lang['goodnews.group_err_save']                                   = 'Beim Speichern der Gruppe ist ein Fehler aufgetreten.';
                                                            
$_lang['goodnews.category']                                         = 'Kategorie';
$_lang['goodnews.categories']                                       = 'Kategorien';
$_lang['goodnews.categories_management_desc']                       = 'Verwalten Sie Ihre Mailing-Kategorien hier.';
$_lang['goodnews.category_create']                                  = 'Kategorie erstellen';
$_lang['goodnews.category_name']                                    = 'Kategoriename';
$_lang['goodnews.category_description']                             = 'Beschreibung';
$_lang['goodnews.category_membercount']                             = 'Abonnenten';
$_lang['goodnews.category_usergroup']                               = 'Gruppe';
$_lang['goodnews.category_belongs_to_usergroup']                    = 'Gehört zur Gruppe';
$_lang['goodnews.category_public']                                  = '&Ouml;ffentlich';
$_lang['goodnews.categories_none']                                  = 'Keine Kategorien';
$_lang['goodnews.category_remove']                                  = 'Kategorie entfernen';
$_lang['goodnews.category_update']                                  = 'Kategorie bearbeiten';
$_lang['goodnews.category_remove_confirm']                          = 'Sind Sie sicher, dass Sie diese Kategorie entfernen m&ouml;chten?<br /><br />Beim Entfernen einer Kategorie werden auch die Abonnements aller Mitglieder dieser Kategorie entfernt.<br /><br /><strong>Dies kann nicht r&uuml;ckg&auml;ngig gemacht werden!</strong>';
$_lang['goodnews.category_err_ns_name']                             = 'Bitte geben Sie einen Namen f&uuml;r die Kategorie an.';
$_lang['goodnews.category_err_ns_group']                            = 'Bitte w&auml;hlen Sie eine Gruppe f&uuml;r die Kategorie aus.';
$_lang['goodnews.category_err_ae']                                  = 'Eine Kategorie mit diesem Namen existiert bereits.';
$_lang['goodnews.category_err_nf']                                  = 'Kategorie nicht gefunden.';
$_lang['goodnews.category_err_ns']                                  = 'Kategorie nicht angegeben.';
$_lang['goodnews.category_err_remove']                              = 'Beim Entfernen der Kategorie ist ein Fehler aufgetreten.';
$_lang['goodnews.category_err_save']                                = 'Beim Speichern der Kategorie ist ein Fehler aufgetreten.';

$_lang['goodnews.subscribers']                                      = 'Abonnenten';
$_lang['goodnews.subscribers_desc']                                 = 'Verwalten Sie Ihre Mailing-Abonnenten hier. <span class="gon-no-subscriptions">Ausgegraute Benutzer</span> haben keine GoodNews Meta-Daten zugewiesen oder derzeit keine Abonnements aktiviert. Neue Abonnenten werden &uuml;ber Benutzer Abonnements, manuell &uuml;ber die MODx Benutzerverwaltung oder per Import erstellt.';
$_lang['goodnews.modx_user_create']                                 = 'MODx Benutzer erstellen';
$_lang['goodnews.subscribers_user_group']                           = 'Abonnenten Gruppe';
$_lang['goodnews.subscribers_user_group_filter']                    = 'Filtern nach Gruppe...';
$_lang['goodnews.subscribers_choose_user_group']                    = '(Abonnenten Gruppe wählen)';
$_lang['goodnews.subscriber_username']                              = 'Benutzername';
$_lang['goodnews.subscriber_groups_categories']                     = 'Abonnierte Gruppen und Kategorien';
$_lang['goodnews.subscriber_fullname']                              = 'Vollst&auml;ndiger Name';
$_lang['goodnews.subscriber_email']                                 = 'Email Adresse';
$_lang['goodnews.subscriber_mailing_groups']                        = 'Mailing Gruppen';
$_lang['goodnews.subscriber_mailing_categories']                    = 'Mailing Kategorien';
$_lang['goodnews.subscriber_subscribed_on']                         = 'Abonniert am';
$_lang['goodnews.subscriber_active']                                = 'Aktiv';
$_lang['goodnews.subscriber_testdummy']                             = 'Tester';
$_lang['goodnews.subscriber_ip']                                    = 'IP Adresse';
$_lang['goodnews.subscriber_ip_unknown']                            = 'unbekannt';
$_lang['goodnews.subscriber_ip_imported']                           = 'importiert';
$_lang['goodnews.subscriber_ip_manually']                           = 'manuell';
$_lang['goodnews.subscriber_soft_bounces']                          = 'SB';
$_lang['goodnews.subscriber_hard_bounces']                          = 'HB';
$_lang['goodnews.subscriber_testdummy_filter']                      = 'Test Empfänger?';
$_lang['goodnews.subscriber_select_as_testdummy']                   = 'Abonnent ist Empfänger von Test-Mailings und Newsletters';
$_lang['goodnews.subscribers_none']                                 = 'Keine Abonnenten';
$_lang['goodnews.subscribers_no_group']                             = 'Keine Gruppe zugewiesen';
$_lang['goodnews.subscriber_update']                                = 'Abonnements bearbeiten';
$_lang['goodnews.subscriber_remove_subscriptions']                  = 'Alle Abonnements entfernen';
$_lang['goodnews.subscriber_remove_meta_data']                      = 'Alle GoodNews Meta-Daten entfernen';
$_lang['goodnews.subscriber_remove_meta_data_confirm']              = 'Sind Sie sicher, dass Sie alle GoodNews Meta-Daten dieses Benutzers entfernen m&ouml;chten?';
$_lang['goodnews.subscriber_remove_subscriptions_confirm']          = 'Sind Sie sicher, dass Sie alle Abonnements dieses Benutzers entfernen m&ouml;chten?';
$_lang['goodnews.user_update']                                      = 'Benutzer bearbeiten (MODx Benutzer Tabelle)';

$_lang['goodnews.settings']                                         = 'GoodNews - Einstellungen';
$_lang['goodnews.settings_save_button']                             = 'Einstellungen Speichern';
$_lang['goodnews.settings_close_button']                            = 'Schließen';
$_lang['goodnews.settings_general_tab']                             = 'Allgemein';
$_lang['goodnews.settings_general_tab_desc']                        = 'Bearbeiten Sie die allgemeinen GoodNews Einstellungen hier.';
$_lang['goodnews.settings_container_tab']                           = 'Container';
$_lang['goodnews.settings_container_tab_desc']                      = 'Verwalten Sie geschützte Einstellungen Ihrer GoodNews Resource Container hier. Die Tabelle listet die im Resourcen Baum verfügbaren GoodNews Container.';
$_lang['goodnews.settings_bounceparsingrules_tab']                  = 'Bounce Parsing Regeln';
$_lang['goodnews.settings_bounceparsingrules_tab_desc']             = 'Bearbeiten Sie die Regeln zum Parsen von Bounce-Nachrichten hier.';
$_lang['goodnews.settings_system_tab']                              = 'System';
$_lang['goodnews.settings_system_tab_desc']                         = 'Bearbeiten Sie die GoodNews System Einstellungen hier.';
$_lang['goodnews.settings_about_tab']                               = 'Über GoodNews';
$_lang['goodnews.settings_about_tab_desc']                          = 'GoodNews System Informationen.';
$_lang['goodnews.settings_test_subject_prefix']                     = 'Präfix für Betreff bei Test-Mailings';
$_lang['goodnews.settings_test_subject_prefix_desc']                = 'Definieren Sie einen Text der Betreffzeilen von Test-Mailings vorangestellt wird.';
$_lang['goodnews.settings_mailing_bulk_size']                       = 'Mailing-Paket Größe (1-100)';
$_lang['goodnews.settings_mailing_bulk_size_desc']                  = 'Definiert die Anzahl der Mails, die von einem Sendeprozess versendet werden.';
$_lang['goodnews.settings_mails_per_bulk']                          = ' Mails pro Durchgang';
$_lang['goodnews.settings_worker_process_limit']                    = 'Maximale Anzahl der Sendeprozesse (1-10)';
$_lang['goodnews.settings_worker_process_limit_desc']               = 'Definiert die maximale Anzahl der parallelen Sendeprozesse, die verwendet werden.';
$_lang['goodnews.settings_process_max']                             = ' Prozesse maximal';
$_lang['goodnews.settings_worker_process_active']                   = 'Sendeprozess aktiviert';
$_lang['goodnews.settings_worker_process_active_desc']              = 'Diese Einstellung kann verwendet werden, um den Sendeprozess zu deaktivieren. Dies bedeutet Job Scheduler wie z.B. Cron können keinen neuen Prozess auslösen.';
$_lang['goodnews.settings_cron_security_key']                       = 'Cron Sicherheits-Schlüssel';
$_lang['goodnews.settings_cron_security_key_desc']                  = 'Geben Sie eine zufällige (passwort-ähnliche) Zeichenkette ein, welche für die Cron CLI Authentifizierung verwendet wird. Sie müssen Ihren Cron-Job mit dem zusätzlichen CLI Parameter <strong>sid=[ihr cron sicherheits schlüssel]</strong> konfigurieren. Aus Sicherheitsgründen wird dringend empfohlen einen Schlüssen zu verwenden. Lassen Sie das Feld leer, wenn Sie keine Möglichkeit haben den Cron-Job mit einem zusätzlichen Parameter zu konfigurieren.';
$_lang['goodnews.settings_admin_groups']                            = 'GoodNews Administrator Gruppen';
$_lang['goodnews.settings_admin_groups_desc']                       = 'Komma separierte Liste von MODx Benutzer-Gruppen, die zur Bearbeitung der GoodNews Einstellungen berechtigt sind.';
$_lang['goodnews.settings_auto_cleanup_subscriptions']              = 'Abonnements automatisch aufräumen';
$_lang['goodnews.settings_auto_cleanup_subscriptions_desc']         = 'Wenn eingeschaltet, werden nicht aktivierte Abonnements inklusive zugehöriger MODX Benutzerkonten automatisch entfernt. Dies betrifft nur MODX Benutzerkonten, die über GoodNews Metadaten verfügen! MODX Benutzerkonten, welche MODX Benutzergruppen zugeordnet sind, bleiben unberührt!';
$_lang['goodnews.settings_auto_cleanup_subscriptions_ttl']          = 'Zeitspanne für autom. Entfernen';
$_lang['goodnews.settings_auto_cleanup_subscriptions_ttl_desc']     = 'Zeit in Minuten, nach der nicht aktivierte Abonnements automatisch entfernt werden (Standard: 360 Minuten).';

$_lang['goodnews.settings_containers_none']                         = 'Keine GoodNews Resource Container gefunden';
$_lang['goodnews.settings_container_id']                            = 'ID';
$_lang['goodnews.settings_container_context_key']                   = 'Context';
$_lang['goodnews.settings_container_pagetitle']                     = 'Resource Container Name';
$_lang['goodnews.settings_container_editor_groups']                 = 'Container Editor Gruppen';
$_lang['goodnews.settings_container_editor_groups_desc']            = 'Komma separierte Liste von MODx Benutzer-Gruppen, die Zugriff auf diesen GoodNews Container haben.';
$_lang['goodnews.settings_container_mail_from']                     = 'Absender E-Mail Adresse (für Bounce Nachrichten)';
$_lang['goodnews.settings_container_mail_from_desc']                = 'Die E-Mail Adresse, die für den Mailversand aus diesem Container verwendet wird sowie für den Empfang von Bounce Nachrichten (Non Delivery Notifications).';
$_lang['goodnews.settings_container_mail_from_name']                = 'Absender Name';
$_lang['goodnews.settings_container_mail_from_name_desc']           = 'Der Absender Name, der für den Mailversand aus diesem Container verwendet wird.';
$_lang['goodnews.settings_container_mail_reply_to']                 = 'Antwort E-Mail Adresse';
$_lang['goodnews.settings_container_mail_reply_to_desc']            = 'Die E-Mail Adresse, die für den Empfang von Antworten auf Mails aus diesem Container verwendet wird.';
$_lang['goodnews.settings_container_mail_bouncehandling']           = 'Automatisches Bounce Handling';
$_lang['goodnews.settings_container_mail_bouncehandling_desc']      = 'Legt fest ob Bounce Nachrichten automatisch ausgewertet und verarbeitet werden sollen.';
$_lang['goodnews.settings_container_mail_service']                  = 'Kontotyp';
$_lang['goodnews.settings_container_mail_service_desc']             = 'Der Kontotyp des Mailservices (imap or pop3).';
$_lang['goodnews.settings_container_mail_mailhost']                 = 'Mailserver Name';
$_lang['goodnews.settings_container_mail_mailhost_desc']            = 'Servername für eintreffende E-Mails.';
$_lang['goodnews.settings_container_mail_mailbox_username']         = 'Mailbox Benutzername';
$_lang['goodnews.settings_container_mail_mailbox_username_desc']    = 'Der Benutzername für den Zugriff auf die Mailbox.';
$_lang['goodnews.settings_container_mail_mailbox_password']         = 'Mailbox Kennwort';
$_lang['goodnews.settings_container_mail_mailbox_password_desc']    = 'Das Kennwort für den Zugriff auf die Mailbox.';
$_lang['goodnews.settings_container_mail_boxname']                  = 'Mailbox Name';
$_lang['goodnews.settings_container_mail_boxname_desc']             = 'Der Ordnername der Mailbox (Standard: INBOX).';
$_lang['goodnews.settings_container_mail_port']                     = 'Portnummer';
$_lang['goodnews.settings_container_mail_port_desc']                = 'Die Portnummer für den Mailboxzugriff (Standard: 143).';
$_lang['goodnews.settings_container_mail_service_option']           = 'Service Option';
$_lang['goodnews.settings_container_mail_service_option_desc']      = 'Die Service Option für den Mailboxzugriff (Standard: notls).';
$_lang['goodnews.settings_container_softbounced_msg_action']        = 'Aktion für Soft Bounce Nachrichten';
$_lang['goodnews.settings_container_softbounced_msg_action_desc']   = 'Was soll mit Soft Bounce Nachrichten nach dem Verarbeiten geschehen?';
$_lang['goodnews.settings_container_softbounces_mailbox']           = 'Mailordner für Soft Bounce Nachrichten';
$_lang['goodnews.settings_container_softbounces_mailbox_desc']      = 'Name des Mailordners für das Verschieben von Soft Bounce Nachrichten.';
$_lang['goodnews.settings_container_max_softbounces']               = 'Max. Anzahl von Soft Bounces';
$_lang['goodnews.settings_container_max_softbounces_desc']          = 'Maximale Anzahl von Soft Bounce Nachrichten pro Abonnenten';
$_lang['goodnews.settings_container_max_softbounces_action']        = 'Aktion bei max. Anzahl von Soft Bounces';
$_lang['goodnews.settings_container_max_softbounces_action_desc']   = 'Aktion für Abonnenten bei Erreichen der maximalen Anzahl von Soft Bounces?';
$_lang['goodnews.settings_container_hardbounced_msg_action']        = 'Aktion für Hard Bounce Nachrichten';
$_lang['goodnews.settings_container_hardbounced_msg_action_desc']   = 'Was soll mit Hard Bounce Nachrichten nach dem Verarbeiten geschehen?';
$_lang['goodnews.settings_container_hardbounces_mailbox']           = 'Mailordner für Hard Bounce Nachrichten';
$_lang['goodnews.settings_container_hardbounces_mailbox_desc']      = 'Name des Mailordners für das Verschieben von Hard Bounce Nachrichten.';
$_lang['goodnews.settings_container_max_hardbounces']               = 'Max. Anzahl von Hard Bounces';
$_lang['goodnews.settings_container_max_hardbounces_desc']          = 'Maximale Anzahl von Hard Bounce Nachrichten pro Abonnenten';
$_lang['goodnews.settings_container_max_hardbounces_action']        = 'Aktion bei max. Anzahl von Hard Bounces';
$_lang['goodnews.settings_container_max_hardbounces_action_desc']   = 'Aktion für Abonnenten bei Erreichen der maximalen Anzahl von Hard Bounces?';
$_lang['goodnews.settings_container_notclassified_msg_action']      = 'Aktion für unklassifizierte Nachrichten';
$_lang['goodnews.settings_container_notclassified_msg_action_desc'] = 'Was soll mit unklassifizierten Nachrichten geschehen?';
$_lang['goodnews.settings_container_notclassified_mailbox']         = 'Mailordner für unklassifizierte Nachrichten';
$_lang['goodnews.settings_container_notclassified_mailbox_desc']    = 'Name des Mailordners für das Verschieben von unklassifizierten Nachrichten.';

$_lang['goodnews.settings_container_mail_pop3']                     = 'POP3';
$_lang['goodnews.settings_container_mail_imap']                     = 'IMAP';
$_lang['goodnews.settings_container_mail_none']                     = 'None';
$_lang['goodnews.settings_container_mail_tls']                      = 'TLS';
$_lang['goodnews.settings_container_mail_notls']                    = 'NO TLS';
$_lang['goodnews.settings_container_mail_ssl']                      = 'SSL';
$_lang['goodnews.settings_container_softbounced_msg_move']          = 'Verschieben';
$_lang['goodnews.settings_container_softbounced_msg_delete']        = 'Löschen';
$_lang['goodnews.settings_container_hardbounced_msg_move']          = 'Verschieben';
$_lang['goodnews.settings_container_hardbounced_msg_delete']        = 'Löschen';
$_lang['goodnews.settings_container_soft_subscriber_disable']       = 'Abonnent deaktivieren';
$_lang['goodnews.settings_container_soft_subscriber_delete']        = 'Abonnent löschen';
$_lang['goodnews.settings_container_hard_subscriber_disable']       = 'Abonnent deaktivieren';
$_lang['goodnews.settings_container_hard_subscriber_delete']        = 'Abonnent löschen';
$_lang['goodnews.settings_container_notclassified_msg_move']        = 'Verschieben';
$_lang['goodnews.settings_container_notclassified_msg_delete']      = 'Löschen';

$_lang['goodnews.settings_container_update']                        = 'Container Einstellungen bearbeiten';
$_lang['goodnews.settings_container_tab_general']                   = 'Allgemein';
$_lang['goodnews.settings_container_tab_bouncemailbox']             = 'Bounce Mailbox';
$_lang['goodnews.settings_container_tab_bouncerules']               = 'Bounce Regeln';
$_lang['goodnews.settings_container_tab_notclassified_rules']       = 'Unklassifiziert Regeln';
$_lang['goodnews.settings_container_err_ns_editor_groups']          = 'Bitte geben Sie mindestens eine Container Editor Gruppe an.';
$_lang['goodnews.settings_container_err_ns_mail_from']              = 'Bitte geben Sie die Absender E-Mail Adresse an (für Bounce Messages).';
$_lang['goodnews.settings_container_err_ns_mail_from_name']         = 'Bitte geben Sie den Absender Namen an.';
$_lang['goodnews.settings_container_err_ns_mail_reply_to']          = 'Bitte geben Sie die Antwort E-Mail Adresse an.';
$_lang['goodnews.settings_container_err_mailbox_connection_failed'] = 'Verbindung zur Bounce Mailbox konnte nicht hergestellt werden! Bitte überprüfen Sie die Einstellungen.';

$_lang['goodnews.import']                                           = 'GoodNews - Import';
$_lang['goodnews.import_button']                                    = 'Import';
$_lang['goodnews.import_close_button']                              = 'Schließen';
$_lang['goodnews.import_subscribers_tab']                           = 'Abonnenten';
$_lang['goodnews.import_subscribers_tab_desc']                      = 'Import von Abonnenten in die MODX Benutzer Datenbank. ACHTUNG: Bitte erstellen sie ein Backup Ihrer MODX Datenbank bevor Sie mit dem Import-Vorgang beginnen!';
$_lang['goodnews.import_subscribers_csvfile']                       = 'CSV Datei';
$_lang['goodnews.import_subscribers_csvfile_desc']                  = 'Auswahl einer CSV Datei, welche die zu importierenden Benutzerdatensätze enthält. Die Datei wird auf den Server hochgeladen.<br><br><strong>Erforderliches CSV Format</strong>:<br>Keine Kopfzeile! | Erstes Feld = email,Zweites Feld = fullname';
$_lang['goodnews.import_subscribers_csvfile_button']                = 'Datei wählen...';
$_lang['goodnews.import_subscribers_batchsize']                     = 'Batch Größe';
$_lang['goodnews.import_subscribers_batchsize_desc']                = 'Anzahl der Datensätze, die importiert werden sollen. Wird 0 eingetragen, werden alle Datensätze importiert. Standard ist 0.';
$_lang['goodnews.import_subscribers_delimiter']                     = 'Feld-Trennzeichen';
$_lang['goodnews.import_subscribers_delimiter_desc']                = 'Legen Sie das Feld-Trennzeichen fest (nur ein Zeichen). Standard ist ,';
$_lang['goodnews.import_subscribers_enclosure']                     = 'Feld-Begrenzerzeichen';
$_lang['goodnews.import_subscribers_enclosure_desc']                = 'Legen Sie das Feld-Begrenzerzeichen fest (nur ein Zeichen). Standard ist "';
$_lang['goodnews.import_subscribers_assign_grpcat']                 = 'Mailing Gruppen und Kategorien zuweisen';
$_lang['goodnews.import_subscribers_grpcat']                        = 'Mailing Gruppen und Kategorien';
$_lang['goodnews.import_subscribers_button_start']                  = 'Import starten';
$_lang['goodnews.import_subscribers_status']                        = 'Import Status';
$_lang['goodnews.import_subscribers_msg_importing']                 = 'Importiere...';
$_lang['goodnews.import_subscribers_msg_successfull']               = 'Import erfolgreich';
$_lang['goodnews.import_subscribers_msg_failed']                    = 'Import fehlgeschlagen';
$_lang['goodnews.import_subscribers_log_prep_csv_import']           = 'Bereite CSV Import vor...';
$_lang['goodnews.import_subscribers_log_importing_csv']             = 'Importiere CSV Datei';
$_lang['goodnews.import_subscribers_log_batchsize']                 = 'Batch Größe: ';
$_lang['goodnews.import_subscribers_log_imported_subscr']           = 'Importiert: ';
$_lang['goodnews.import_subscribers_log_err_subscr_failed']         = 'Nicht importiert. Speichern fehlgeschlagen: ';
$_lang['goodnews.import_subscribers_log_err_subscr_ae']             = 'Nicht importiert. Existiert bereits: ';
$_lang['goodnews.import_subscribers_log_err_subscr_data']           = 'Nicht importiert. Datenformat falsch: ';
$_lang['goodnews.import_subscribers_log_err_email_invalid']         = 'Nicht importiert. Email Adresse ungültig: ';
$_lang['goodnews.import_subscribers_log_ns_csvfile']                = 'Bitte wählen Sie eine CSV Datei.';
$_lang['goodnews.import_subscribers_log_wrong_filetype']            = 'Nur CSV Dateien sind zulässig.';
$_lang['goodnews.import_subscribers_log_no_class']                  = 'GoodNewsImportSubscribers Klasse konnte nicht instanziert werden.';
$_lang['goodnews.import_subscribers_log_ns_batchsize']              = 'Bitte geben Sie eine Batch Größe an.';
$_lang['goodnews.import_subscribers_log_ns_delimiter']              = 'Bitte geben Sie das Feld-Trennzeichen an.';
$_lang['goodnews.import_subscribers_log_ns_enclosure']              = 'Bitte geben Sie das Feld-Begrenzerzeichen an.';
$_lang['goodnews.import_subscribers_log_ns_grpcat']                 = 'Bitte wählen Sie mindestens eine GoodNews Gruppe (oder mehrere Gruppen und Kategorien) aus.';
$_lang['goodnews.import_subscribers_log_finished']                  = 'Import abgeschlossen. Erfolgreich importierte Abonnenten: ';
$_lang['goodnews.import_subscribers_log_failed']                    = 'Import fehlgeschlagen. Bitte überprüfen Sie die Fehlermeldungen.';
$_lang['goodnews.import_subscribers_log_err_open_csvfile']          = 'Fehler beim Öffnen der CSV Datei.';

$_lang['setting_goodnews.current_container']                        = 'Aktueller GoodNews Container';
$_lang['setting_goodnews.current_container_desc']                   = 'ID des aktuell ausgewählten GoodNews Containers.';
$_lang['setting_goodnews.test_subject_prefix']                      = 'Präfix für Betreff bei Test-Mailings';
$_lang['setting_goodnews.test_subject_prefix_desc']                 = 'Definieren Sie einen Text der Betreffzeilen von Test-Mailings vorangestellt wird.';
$_lang['setting_goodnews.mailing_bulk_size']                        = 'Mailing-Paket Größe';
$_lang['setting_goodnews.mailing_bulk_size_desc']                   = 'Definiert die Anzahl der Mails, die in einem Durchgang versendet werden.';
$_lang['setting_goodnews.worker_process_limit']                     = 'Maximale Anzahl der Sendeprozesse (1-10)';
$_lang['setting_goodnews.worker_process_limit_desc']                = 'Definiert die maximale Anzahl der parallelen Sendeprozesse, die verwendet werden.';
$_lang['setting_goodnews.worker_process_active']                    = 'Sendeprozess aktiviert';
$_lang['setting_goodnews.worker_process_active_desc']               = 'Darf der Cron Job den GoodNews Sendeprozess starten? Diese Einstellung kann verwendet werden, um den Cron Trigger temporär zu deaktivieren.';
$_lang['setting_goodnews.admin_groups']                             = 'GoodNews Administrator Gruppen';
$_lang['setting_goodnews.admin_groups_desc']                        = 'Komma separierte Liste von MODx Benutzer-Gruppen, die zur Bearbeitung der GoodNews Einstellungen berechtigt sind.';
$_lang['setting_goodnews.auto_cleanup_subscriptions']               = 'Abonnements automatisch aufräumen';
$_lang['setting_goodnews.auto_cleanup_subscriptions_desc']          = 'Wenn aktiviert, werden inaktive bzw. nicht aktivierte Abonnements inklusive zugehöriger MODX Benutzerkonten automatisch entfernt. Dies betrifft nur MODX Benutzerkonten, die mit GoodNews Metadaten verknüpft sind! MODX Benutzerkonten, welche MODX Benutzergruppen zugeordnet sind, bleiben unberührt!';
$_lang['setting_goodnews.auto_cleanup_subscriptions_ttl']           = 'Zeitspanne für autom. Entfernen';
$_lang['setting_goodnews.auto_cleanup_subscriptions_ttl_desc']      = 'Zeit in Minuten, nach der nicht aktivierte Abonnements automatisch entfernt werden (Standard: 360 Minuten).';
$_lang['setting_goodnews.cron_security_key']                        = 'Cron Sicherheits-Schlüssel';
$_lang['setting_goodnews.cron_security_key_desc']                   = 'Geben Sie eine zufällige (passwort-ähnliche) Zeichenkette ein, welche für die Cron CLI Authentifizierung verwendet wird. Sie müssen Ihren Cron-Job mit dem zusätzlichen CLI Parameter <strong>sid=[ihr cron sicherheits schlüssel]</strong> konfigurieren. Aus Sicherheitsgründen wird dringend empfohlen einen Schlüssen zu verwenden. Lassen Sie das Feld leer, wenn Sie keine Möglichkeit haben den Cron-Job mit einem zusätzlichen Parameter zu konfigurieren.';
$_lang['setting_goodnews.default_container_template']               = 'Standard GoodNews-Container Template';
$_lang['setting_goodnews.default_container_template_desc']          = 'Das Template, welches standardmäßig verwendet wird, wenn ein GoodNews-Container erstellt wird.';
$_lang['setting_goodnews.debug']                                    = 'GoodNews Debug Modus';
$_lang['setting_goodnews.debug_desc']                               = 'Debug Modus für GoodNews aktivieren / deaktivieren. Wenn aktiviert, werden zusätzliche Informationen im MODX Fehlerprotokoll ausgegeben.';
