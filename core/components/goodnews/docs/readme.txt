--------------------
Extra: GoodNews
--------------------
Version: 1.1.5-pl
Author: bitego <office@bitego.com> (Martin Gartner, Franz Gallei)
--------------------
 
An integrated group and newsletter mailing system.

GoodNews is a powerful integrated group and newsletter mailing 
system for the content management framework MODX Revolution.

With GoodNews you can easily create mailings or newsletters 
and have them sent to your subscribers or internal MODX user 
groups automatically right from your MODX back-end. GoodNews 
is built with a lot of passion for details, a polished interface 
and easy operation. The sending process of GoodNews is lightning 
fast due to its underlying multithreaded sending system.

Reqirements:

- MODx 2.2 and above
- Cron
- PHP Imap Extension (for automatic bounce handling)

Feel free to suggest ideas/improvements/bugs on our website:

http://www.bitego.com/extras/goodnews/goodnews-support.html

