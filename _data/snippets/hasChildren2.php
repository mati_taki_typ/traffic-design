id: 75
source: 5
name: hasChildren2
properties: null
static: 1
static_file: snippets/hasChildren2.php

-----

$docId = isset($id) ? intval($id) : $modx->resource->get('id');


if($docId==0){
	return 0; //dla resourców bez parenta zwraca 0 (nie ma menu
}
else{

		
		$document = $modx->getObject('modResource', $docId);
		
		 $tv_menu = $document->getTVValue($tv);
		
		 $hasChildren = $document->hasChildren();
		 
		 //return "docID:".$docId."|".$tv_menu ."|".$hasChildren;
		
		 
		 
		 if($tv_menu==1 &&  $hasChildren>0){ 
			//jeśli ma dzieci i menu_gorne zaznaczone
			return 1;
	
			
		}
	
		elseif(empty($tv_menu) &&  $hasChildren>0){     
		
			//jeśli ma dzieci i menu_gorne niezaznaczone
		  	return 2;
		
			
		}
		
		//jeśli nie ma dzieci
		else{
			
			 return 0;
			}
}