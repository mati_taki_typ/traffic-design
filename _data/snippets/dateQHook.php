id: 107
name: dateQHook
description: 'Szuka wg przedziału daty: 1m, 3m, 1y, all'
category: AdvSearch
properties: null

-----

/**
 * AdvSearch
 *
 * Copyright 2012 by Coroico <coroico@wangba.fr>
 *
 * QueryHook for Faceted search demo 8
 *
 */

$update = (isset($_REQUEST['update'])) ? strip_tags($_REQUEST['update']) : 'anytime';

// filter search results by published date
if ($update == '1month') $refdate = mktime(0, 0, 0, date('m')-1  , date('d') , date('Y'));
else if ($update == '3months') $refdate = mktime(0, 0, 0, date('m')-3  , date('d') , date('Y'));
else if ($update == '1year') $refdate = mktime(0, 0, 0, date('m')  , date('d') , date('Y')-1);

if (isset($refdate)) {
	$andConditions = array(
		'modResource.publishedon:>' => $refdate . ':integer'
	);

	$qhDeclaration = array(
		'qhVersion' => '1.2',
		'andConditions' => $andConditions
	);

	$hook->setQueryHook($qhDeclaration);

}
return true;