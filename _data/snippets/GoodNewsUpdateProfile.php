id: 118
name: GoodNewsUpdateProfile
description: 'Allows front-end updating of a users GoodNews profile.'
category: GoodNews
properties: 'a:26:{s:6:"errTpl";a:7:{s:4:"name";s:6:"errTpl";s:4:"desc";s:38:"prop_goodnewsupdateprofile.errtpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:37:"<span class="error">[[+error]]</span>";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:11:"useExtended";a:7:{s:4:"name";s:11:"useExtended";s:4:"desc";s:43:"prop_goodnewsupdateprofile.useextended_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:15:"excludeExtended";a:7:{s:4:"name";s:15:"excludeExtended";s:4:"desc";s:47:"prop_goodnewsupdateprofile.excludeextended_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:10:"emailField";a:7:{s:4:"name";s:10:"emailField";s:4:"desc";s:42:"prop_goodnewsupdateprofile.emailfield_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:5:"email";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:8:"preHooks";a:7:{s:4:"name";s:8:"preHooks";s:4:"desc";s:40:"prop_goodnewsupdateprofile.prehooks_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:9:"postHooks";a:7:{s:4:"name";s:9:"postHooks";s:4:"desc";s:41:"prop_goodnewsupdateprofile.posthooks_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:20:"sendUnauthorizedPage";a:7:{s:4:"name";s:20:"sendUnauthorizedPage";s:4:"desc";s:52:"prop_goodnewsupdateprofile.sendunauthorizedpage_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:15:"reloadOnSuccess";a:7:{s:4:"name";s:15:"reloadOnSuccess";s:4:"desc";s:47:"prop_goodnewsupdateprofile.reloadonsuccess_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:9:"submitVar";a:7:{s:4:"name";s:9:"submitVar";s:4:"desc";s:41:"prop_goodnewsupdateprofile.submitvar_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:26:"goodnews-updateprofile-btn";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:10:"successKey";a:7:{s:4:"name";s:10:"successKey";s:4:"desc";s:42:"prop_goodnewsupdateprofile.successkey_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:10:"updsuccess";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:10:"successMsg";a:7:{s:4:"name";s:10:"successMsg";s:4:"desc";s:42:"prop_goodnewsupdateprofile.successmsg_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:8:"validate";a:7:{s:4:"name";s:8:"validate";s:4:"desc";s:40:"prop_goodnewsupdateprofile.validate_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:14:"grpFieldsetTpl";a:7:{s:4:"name";s:14:"grpFieldsetTpl";s:4:"desc";s:46:"prop_goodnewsupdateprofile.grpfieldsettpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:29:"sample.GoodNewsGrpFieldsetTpl";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:10:"grpNameTpl";a:7:{s:4:"name";s:10:"grpNameTpl";s:4:"desc";s:42:"prop_goodnewsupdateprofile.grpnametpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:25:"sample.GoodNewsGrpNameTpl";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:11:"grpFieldTpl";a:7:{s:4:"name";s:11:"grpFieldTpl";s:4:"desc";s:43:"prop_goodnewsupdateprofile.grpfieldtpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:26:"sample.GoodNewsGrpFieldTpl";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:17:"grpFieldHiddenTpl";a:7:{s:4:"name";s:17:"grpFieldHiddenTpl";s:4:"desc";s:49:"prop_goodnewsupdateprofile.grpfieldhiddentpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:32:"sample.GoodNewsGrpFieldHiddenTpl";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:11:"catFieldTpl";a:7:{s:4:"name";s:11:"catFieldTpl";s:4:"desc";s:43:"prop_goodnewsupdateprofile.catfieldtpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:26:"sample.GoodNewsCatFieldTpl";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:17:"catFieldHiddenTpl";a:7:{s:4:"name";s:17:"catFieldHiddenTpl";s:4:"desc";s:49:"prop_goodnewsupdateprofile.catfieldhiddentpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:32:"sample.GoodNewsCatFieldHiddenTpl";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:10:"groupsOnly";a:7:{s:4:"name";s:10:"groupsOnly";s:4:"desc";s:42:"prop_goodnewsupdateprofile.groupsonly_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:13:"includeGroups";a:7:{s:4:"name";s:13:"includeGroups";s:4:"desc";s:45:"prop_goodnewsupdateprofile.includegroups_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:13:"defaultGroups";a:7:{s:4:"name";s:13:"defaultGroups";s:4:"desc";s:45:"prop_goodnewsupdateprofile.defaultgroups_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:17:"defaultCategories";a:7:{s:4:"name";s:17:"defaultCategories";s:4:"desc";s:49:"prop_goodnewsupdateprofile.defaultcategories_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:4:"sort";a:7:{s:4:"name";s:4:"sort";s:4:"desc";s:36:"prop_goodnewsupdateprofile.sort_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:4:"name";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:3:"dir";a:7:{s:4:"name";s:3:"dir";s:4:"desc";s:35:"prop_goodnewsupdateprofile.dir_desc";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:5:"value";s:3:"ASC";s:4:"text";s:16:"opt_goodnews.asc";}i:1;a:2:{s:5:"value";s:4:"DESC";s:4:"text";s:17:"opt_goodnews.desc";}}s:5:"value";s:3:"ASC";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:17:"grpCatPlaceholder";a:7:{s:4:"name";s:17:"grpCatPlaceholder";s:4:"desc";s:49:"prop_goodnewsupdateprofile.grpcatplaceholder_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:15:"grpcatfieldsets";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:17:"placeholderPrefix";a:7:{s:4:"name";s:17:"placeholderPrefix";s:4:"desc";s:49:"prop_goodnewsupdateprofile.placeholderprefix_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}}'

-----

/**
 * GoodNews
 *
 * Copyright 2012 by bitego <office@bitego.com>
 * Based on code from Login add-on
 * Copyright 2010 by Shaun McCormick <shaun@modx.com>
 * Modified by bitego - 10/2013
 *
 * GoodNews is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * GoodNews is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this software; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

/**
 * Snippet which handles updating of subscribers profile.
 *
 * @var modX $modx
 * @var GoodNewsSubscription $goodnewssubscription
 * @var array $scriptProperties
 *
 * @property string $errTpl Chunk name for group fieldset (default: <span class="error">[[+error]]</span>)
 * @property boolean $useExtended Whether or not to set any extra fields in the form to the users extended field. (default: 0 = false)
 * @property string $excludeExtended A comma-separated list of fields to exclude from setting as extended fields.
 * @property string $emailField The name of the field to use for the user's email address. (default: email)
 * @property string $preHooks A comma-separated list of 'hooks' (snippets), that will be executed before the user profile is updated but after validation.
 * @property string $postHooks A comma-separated list of 'hooks' (snippets), that will be executed after the user profile is updated.
 * @property boolean $sendUnauthorizedPage Whether or not to redirect a subscriber to the unauthorized page if his authentication is not verified (default: 0 = false)
 * @property boolean $reloadOnSuccess If true, page will redirect to itself to prevent double-postbacks. Otherwise it will set a success placeholder. (default: 1 = true)
 * @property string $submitVar The name of the form submit button that triggers the submission. (default: goodnews-updateprofile-btn)
 * @property string $successKey The name of the key submitted as url param in case of success (default: updsuccess)
 * @property string $successMsg If page redirects to itself, this message will be set to a placeholder.
 * @property string $validate A comma-separated list of fields to validate. (default: '')
 * @property string $grpFieldsetTpl Chunk name for group fieldset. (default: sample.GoodNewsGrpFieldsetTpl)
 * @property string $grpFieldTpl Chunk name for group checkbox element. (default: sample.GoodNewsGrpFieldTpl)
 * @property string $grpNameTpl Chunk name for group name element. (default: sample.GoodNewsGrpNameTpl)
 * @property string $grpFieldHiddenTpl Chunk name for group input hidden element. (default: sample.GoodNewsGrpFieldHiddenTpl)
 * @property string $catFieldTpl Chunk name for category checkbox element. (default: sample.GoodNewsCatFieldTpl)
 * @property string $catFieldHiddenTpl Chunk name for category input hidden element. (default: sample.GoodNewsCatFieldHiddenTpl)
 * @property boolean $groupsOnly Whether or not the output should only contain groups. (default: 0 = false)
 * @property string $includeGroups Comma separated list of group ids to be used for output. (default: 0 = use all groups)
 * @property string $defaultGroups Comma separated list of group ids which should be preselected. (checked). (default: 0 = none checked)
 * @property string $defaultCategories Comma separated list of category ids which should be preselected (checked). (default: 0 = none checked)
 * @property string $sort Field to sort by for groups/categories. (default: name)
 * @property string $dir Sort direction for groups/categories. (default: ASC)
 * @property string $grpCatPlaceholder The placeholder to set the generatede groups/categories tree to. (default: grpcatfieldsets)
 * @property string $placeholderPrefix The prefix to use for all placeholders set by this snippet. (default: '')
 *
 * @package goodnews
 */

require_once $modx->getOption('goodnews.core_path', null, $modx->getOption('core_path').'components/goodnews/').'model/goodnews/goodnewssubscription.class.php';
$goodnewssubscription = new GoodNewsSubscription($modx, $scriptProperties);

$controller = $goodnewssubscription->loadController('UpdateProfile');
$output = $controller->run($scriptProperties);
return $output;