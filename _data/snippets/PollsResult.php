id: 61
name: PollsResult
description: 'Shows the results of a single poll'
category: Polls
properties: 'a:3:{s:13:"resultLinkVar";a:7:{s:4:"name";s:13:"resultLinkVar";s:4:"desc";s:90:"(Optional) when using resultResource, this is the paramatername the snippet is looking for";s:4:"type";s:7:"integer";s:7:"options";a:0:{}s:5:"value";s:4:"poll";s:7:"lexicon";N;s:4:"area";s:0:"";}s:3:"tpl";a:7:{s:4:"name";s:3:"tpl";s:4:"desc";s:42:"The main result template for the poll view";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:22:"pollsLatestResultOuter";s:7:"lexicon";N;s:4:"area";s:0:"";}s:15:"tplResultAnswer";a:7:{s:4:"name";s:15:"tplResultAnswer";s:4:"desc";s:57:"The result template for the answers inside the outer view";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:23:"pollsLatestResultAnswer";s:7:"lexicon";N;s:4:"area";s:0:"";}}'

-----

/**
 * PollsResult
 * Shows the results of a single poll
 *
 * @author Bert Oost @ OostDesign
 * @copyright Copyright Bert Oost <bert@oostdesign.nl>
 * @package polls
 *
 * TEMPLATES:
 *
 * tpl - The main result template for the poll view
 * tplAnswer - The result template for the answers inside the outer view
 *
 * LINKING:
 *
 * resultLinkVar - (Opt) when using resultResource, this is the paramatername the snippet is looking for
 */
$polls = $modx->getService('polls','Polls',$modx->getOption('polls.core_path',null,$modx->getOption('core_path').'components/polls/').'model/polls/',$scriptProperties);
if (!($polls instanceof Polls)) return '';

// templates
$tpl = $modx->getOption('tpl', $scriptProperties, 'pollsLatestResultOuter');
$tplAnswer = $modx->getOption('tplAnswer', $scriptProperties, 'pollsLatestResultAnswer');

// properties
$resultLinkVar = $modx->getOption('resultLinkVar', $scriptProperties, 'poll');

if(isset($_GET[$resultLinkVar]) && is_numeric($_GET[$resultLinkVar]) && $_GET[$resultLinkVar] > 0) {
  
  // start getting poll results
  $c = $modx->newQuery('modPollQuestion');
  
  $c->innerJoin('modPollAnswer','Answers');
  $c->where(array(
    'modPollQuestion.id:=' => $_GET[$resultLinkVar],
    'modPollQuestion.hide:=' => false,
    "(`modPollQuestion`.`publishdate` <= '".date('Y-m-d H:i:s')."' OR `modPollQuestion`.`publishdate` IS NULL)",
    "(`modPollQuestion`.`unpublishdate` >= '".date('Y-m-d H:i:s')."' OR `modPollQuestion`.`unpublishdate` IS NULL)"
  ));
  
  $result = $modx->getObject('modPollQuestion', $c);
  
  if(!empty($result)) {
    
    $modx->lexicon->load('polls:latestpoll');
  
    $placeholders = $result->toArray();
    $placeholders['totalVotes'] = $result->getTotalVotes();
  
    $category = $result->getOne('Category');
    $placeholders['category_name'] = (!empty($category) && is_object($category)) ? $category->get('name') : '';
    
    $answers = $result->getMany('Answers');
    $answersOutput = '';
    foreach($answers as $idx => $answer) {
      $answerParams = array_merge(
	$answer->toArray(), array(
	  'percent' => $answer->getVotesPercent($placeholders['totalVotes']),
    'idx' => $idx
	)
      );
      $answersOutput .= $modx->getChunk($tplAnswer, $answerParams);
    }
    
    $placeholders['answers'] = $answersOutput;
    
    if($result->hasVoted()) {
      
      $vote = $result->getOne('Logs', array('ipaddress:=' => $_SERVER['REMOTE_ADDR']));
      $placeholders['logdate'] = $vote->get('logdate');
    }
    
    $output = $modx->getChunk($tpl, $placeholders);
    
    return $output;
  }
}

return '';