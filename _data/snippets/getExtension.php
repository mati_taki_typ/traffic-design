id: 84
source: 5
name: getExtension
properties: null
static: 1
static_file: snippets/getExtension.php

-----

$file = basename($file);
$file  = pathinfo($file, PATHINFO_EXTENSION);
return $file;