id: 104
source: 5
name: quipQHook
properties: 'a:0:{}'
static: 1
static_file: snippets/quipQHook.php

-----

/**
 * AdvSearch
 *
 * Copyright 2012 by Coroico <coroico@wangba.fr>
 *
 * Query hook for the quip demo
 *
 */

/*
  Joined classes:

    package - The name of the schema Package to add.
    packagePath - The path to the model/ directory where the package is located. Use {core_path} as generic variable.
    class - The class name of the table you want to search. e.g: quipComment

    and optionally:

    withFields - A comma-separated list of column names where to search.
    fields - A comma-separated list of column names to display. By default same fields name as withFields. An alias could be provided.
    joinCriteria - The SQL condition to join the table you want to search and an other table.
    where - criteria or Array of criteria. Column names should be prefixed by class name and wrapped with backticks. e.g: "`dvdProducts`.`studio` = 'Warner Bros' "

*/

$joined = array(
    array(
        'package' => 'quip',
        'class' => 'quipComment',
        'packagePath' => '{core_path}components/quip/model/',
        'fields' => ' body  ',   // displayed fields
        'withFields' => 'body',                     // search only in body field
        'joinCriteria' => 'quipComment.resource = modResource.id'
    )
);



// set the query hook declaration
$qhDeclaration = array(
    'qhVersion' => '1.2',       
    'joined' => $joined
);

$hook->setQueryHook($qhDeclaration);

return true;