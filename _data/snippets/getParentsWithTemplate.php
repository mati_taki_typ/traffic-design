id: 99
source: 5
name: getParentsWithTemplate
properties: null
static: 1
static_file: snippets/getParentsWithTemplate.php

-----

$parentIds = $modx->getParentIds($doc_id);
//return implode(",",$parentIds);

foreach ($parentIds as $parentid){
	$resource = $modx->getObject('modResource',$parentid);
	$template = $resource->get('template');
	if($template==$template_id){
		//return $parentid;
		return $modx->setPlaceholder('parent_obiekt',$parentid);
	}
}