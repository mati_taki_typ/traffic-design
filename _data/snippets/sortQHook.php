id: 106
name: sortQHook
description: 'AdvSearch Qhok do sortowania wg. typu'
category: AdvSearch
properties: null

-----

/**
 * AdvSearch
 *
 * Copyright 2012 by Coroico <coroico@wangba.fr>
 *
 * QueryHook for Faceted search demo 5
 *
 */

$qhDeclaration = array(
	'sortby' => 'sort'	// âsortâ is the html tag name to catch the array of fields used to sort results
);

$hook->setQueryHook($qhDeclaration);
return true;