id: 98
source: 5
name: getAllParentsIds
properties: null
static: 1
static_file: snippets/getAllParentsIds.php

-----

$parentIds = $modx->getParentIds($doc_id);
return implode(",",$parentIds);