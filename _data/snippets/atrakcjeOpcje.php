id: 78
source: 1
name: atrakcjeOpcje
description: 'Wypisanie wartości do Input options, które pobierane są z MIGX atrakcje'
properties: 'a:0:{}'

-----

/*

Pobieranie wartości z MIGX atrakcji,
Wypisywanie w postaci ||wartosc1||wartosc2||..

*/

$ido = $modx->resource->get('id');
//$ido = 202;
$obj = $modx->getObject('modResource', array('id'=>$ido));
//$obj = $modx->getObject('modResource', array('template'=>'konfiguracjaTemplate'));
$id = $obj->get('id');
$tvs = $obj->getMany('TemplateVars');

foreach ($tvs as $tv) $rawValue=$tv->getValue($id);

$arrJSON = $modx->fromJSON($rawValue);

foreach($arrJSON as $arrJSONDataSet)
{
 foreach($arrJSONDataSet as $key => $value) if ($key='image') $item=$value;
 break;
}
$arrJSON = $modx->fromJSON($item);

foreach($arrJSON as $arrJSONDataSet) foreach($arrJSONDataSet as $k => $v) if ($k=='atrakcja') $output.= '||'.$v;

return $output;