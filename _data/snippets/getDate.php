id: 150
name: getDate
description: 'A simple timestamp retrieval Snippet for MODX Revolution.'
category: getDate
properties: 'a:1:{s:6:"offset";a:7:{s:4:"name";s:6:"offset";s:4:"desc";s:137:"The period of time to add or subtract from the current timestamp. Use relative date/time formats that the strtotime() parser understands.";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:3:"now";s:7:"lexicon";N;s:4:"area";s:0:"";}}'

-----

/**
 * A simple timestamp retrieval Snippet for MODX Revolution.
 *
 * @author David Pede <dev@tasianmedia.com> <https://twitter.com/davepede>
 * @version 1.0.0-pl
 * @released November 22, 2013
 * @since November 22, 2013
 * @package getdate
 *
 * Copyright (C) 2013 David Pede. All rights reserved. <dev@tasianmedia.com>
 *
 * getDate is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 *
 * getDate is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getDate; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

/* set default properties */
$offset = !empty($offset) ? $offset : '';

$output = '';

$output = strtotime("$offset");

return (string) $output;