id: 105
name: perpQHook
description: 'QHook do wyświetlania select box z liczbą znaków'
category: AdvSearch
properties: null

-----

/**
 * AdvSearch
 *
 * Copyright 2012 by Coroico <coroico@wangba.fr>
 *
 *
 */

$perPage = 'ppage';

$qhDeclaration = array(
	'perPage' => $perPage
);

$hook->setQueryHook($qhDeclaration);
return true;