id: 9
name: QuipLatestComments
description: 'An assistance snippet for getting the latest comments for the whole site or a thread or user.'
category: Quip
properties: 'a:18:{s:4:"type";a:7:{s:4:"name";s:4:"type";s:4:"desc";s:24:"quip.prop_late_type_desc";s:4:"type";s:4:"list";s:7:"options";a:4:{i:0;a:2:{s:5:"value";s:3:"all";s:4:"text";s:8:"quip.all";}i:1;a:2:{s:5:"value";s:6:"thread";s:4:"text";s:11:"quip.thread";}i:2;a:2:{s:5:"value";s:6:"family";s:4:"text";s:11:"quip.family";}i:3;a:2:{s:5:"value";s:4:"user";s:4:"text";s:9:"quip.user";}}s:5:"value";s:3:"all";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:3:"tpl";a:7:{s:4:"name";s:3:"tpl";s:4:"desc";s:23:"quip.prop_late_tpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:17:"quipLatestComment";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:6:"thread";a:7:{s:4:"name";s:6:"thread";s:4:"desc";s:26:"quip.prop_late_thread_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:4:"user";a:7:{s:4:"name";s:4:"user";s:4:"desc";s:24:"quip.prop_late_user_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:6:"family";a:7:{s:4:"name";s:6:"family";s:4:"desc";s:26:"quip.prop_late_family_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:9:"stripTags";a:7:{s:4:"name";s:9:"stripTags";s:4:"desc";s:29:"quip.prop_late_striptags_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:10:"dateFormat";a:7:{s:4:"name";s:10:"dateFormat";s:4:"desc";s:30:"quip.prop_late_dateformat_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:21:"%b %d, %Y at %I:%M %p";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:9:"bodyLimit";a:7:{s:4:"name";s:9:"bodyLimit";s:4:"desc";s:29:"quip.prop_late_bodylimit_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";i:30;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:6:"rowCss";a:7:{s:4:"name";s:6:"rowCss";s:4:"desc";s:26:"quip.prop_late_rowcss_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:19:"quip-latest-comment";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:9:"altRowCss";a:7:{s:4:"name";s:9:"altRowCss";s:4:"desc";s:29:"quip.prop_late_altrowcss_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:23:"quip-latest-comment-alt";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:5:"limit";a:7:{s:4:"name";s:5:"limit";s:4:"desc";s:25:"quip.prop_late_limit_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";i:5;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:5:"start";a:7:{s:4:"name";s:5:"start";s:4:"desc";s:25:"quip.prop_late_start_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";i:0;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:6:"sortBy";a:7:{s:4:"name";s:6:"sortBy";s:4:"desc";s:26:"quip.prop_late_sortby_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:9:"createdon";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:11:"sortByAlias";a:7:{s:4:"name";s:11:"sortByAlias";s:4:"desc";s:31:"quip.prop_late_sortbyalias_desc";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:12:"quip.comment";s:5:"value";s:11:"quipComment";}i:1;a:2:{s:4:"text";s:11:"quip.author";s:5:"value";s:6:"Author";}}s:5:"value";s:11:"quipComment";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:7:"sortDir";a:7:{s:4:"name";s:7:"sortDir";s:4:"desc";s:27:"quip.prop_late_sortdir_desc";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:14:"quip.ascending";s:5:"value";s:3:"ASC";}i:1;a:2:{s:4:"text";s:15:"quip.descending";s:5:"value";s:4:"DESC";}}s:5:"value";s:4:"DESC";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:8:"contexts";a:7:{s:4:"name";s:8:"contexts";s:4:"desc";s:28:"quip.prop_late_contexts_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:13:"toPlaceholder";a:7:{s:4:"name";s:13:"toPlaceholder";s:4:"desc";s:33:"quip.prop_late_toplaceholder_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:17:"placeholderPrefix";a:7:{s:4:"name";s:17:"placeholderPrefix";s:4:"desc";s:37:"quip.prop_late_placeholderprefix_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:11:"quip.latest";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}}'

-----

/**
 * Quip
 *
 * Copyright 2010-11 by Shaun McCormick <shaun@modx.com>
 *
 * This file is part of Quip, a simple commenting component for MODx Revolution.
 *
 * Quip is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * Quip is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Quip; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package quip
 */
/**
 * QuipLatestComments
 *
 * Gets latest comments in a thread or by a user.
 *
 * @var modX $modx
 * @var array $scriptProperties
 * @var Quip $quip
 * 
 * @name QuipLatestComments
 * @author Shaun McCormick <shaun@modx.com>
 * @package quip
 */
$quip = $modx->getService('quip','Quip',$modx->getOption('quip.core_path',null,$modx->getOption('core_path').'components/quip/').'model/quip/',$scriptProperties);
if (!($quip instanceof Quip)) return '';
$quip->initialize($modx->context->get('key'));
$controller = $quip->loadController('LatestComments');
$output = $controller->run($scriptProperties);
return $output;