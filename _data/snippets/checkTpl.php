id: 113
name: checkTpl
description: 'Sprawdza czy dany res posiada template Articles'
category: templates
properties: null

-----

$resource = $modx->getObject('modResource', $id);  
$templateId = $resource->get('template');

return $templateId;