id: 85
name: getReqParam
description: 'Grabs a value from a PHP Superglobal array.'
properties: 'a:2:{s:4:"name";a:7:{s:4:"name";s:4:"name";s:4:"desc";s:25:"The name of the variable.";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:4:"type";a:7:{s:4:"name";s:4:"type";s:4:"desc";s:35:"The superglobal array to grab from.";s:4:"type";s:4:"list";s:7:"options";a:8:{i:0;a:2:{s:4:"text";s:3:"GET";s:5:"value";s:3:"GET";}i:1;a:2:{s:4:"text";s:4:"POST";s:5:"value";s:4:"POST";}i:2;a:2:{s:4:"text";s:7:"REQUEST";s:5:"value";s:7:"REQUEST";}i:3;a:2:{s:4:"text";s:7:"SESSION";s:5:"value";s:7:"SESSION";}i:4;a:2:{s:4:"text";s:6:"SERVER";s:5:"value";s:6:"SERVER";}i:5;a:2:{s:4:"text";s:3:"ENV";s:5:"value";s:3:"ENV";}i:6;a:2:{s:4:"text";s:6:"COOKIE";s:5:"value";s:6:"COOKIE";}i:7;a:2:{s:4:"text";s:5:"FILES";s:5:"value";s:5:"FILES";}}s:5:"value";s:3:"GET";s:7:"lexicon";N;s:4:"area";s:0:"";}}'

-----

/**
 * getReqParam
 *
 * Copyright 2010 by Shaun McCormick <shaun@collabpad.com>
 *
 * getReqParam is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * getReqParam is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getReqParam; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @package getreqparam
 */
/**
 * Returns a parameter from the php request object or other globals.
 *
 * @author Shaun McCormick <shaun@collabpad.com>
 * @license GPLv2
 * @param string $name The name of the variable.
 * @param string $type The type of request. Defaults to GET.
 * @package getreqparam
 *
 * Works with Revolution 2.0.0.
 */
if (empty($modx) || empty($scriptProperties['name']) || empty($scriptProperties['type'])) return null;

if (isset($x)) return  strstr($_SERVER['QUERY_STRING'], $x) ? 'class="active"' : null;


$name = $scriptProperties['name'];
$type = !empty($scriptProperties['type']) ? $scriptProperties['type'] : 'GET';

switch ($type) {
    case 'SERVER':
        return isset($_SERVER[$name]) ? $_SERVER[$name] : null;
        break;
    case 'POST':
        return isset($_POST[$name]) ? $_POST[$name] : null;
        break;
    case 'FILES':
        return isset($_FILES[$name]) ? $_FILES[$name] : null;
        break;
    case 'COOKIE':
        return isset($_COOKIE[$name]) ? $_COOKIE[$name] : null;
        break;
    case 'SESSION':
        return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
        break;
    case 'REQUEST':
        return isset($_REQUEST[$name]) ? $_REQUEST[$name] : null;
        break;
    case 'GET':
    default:
        return isset($_GET[$name]) ? $_GET[$name] : null;
        break;
}
return null;