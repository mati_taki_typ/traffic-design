id: 100
name: AdvSearchForm
description: 'AdvSearchForm snippet for AdvSearch.'
category: AdvSearch
properties: 'a:19:{s:6:"addCss";a:7:{s:4:"name";s:6:"addCss";s:4:"desc";s:35:"advsearch.advsearchform_addCss_desc";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:2:"No";s:5:"value";i:0;}i:1;a:2:{s:4:"text";s:3:"Yes";s:5:"value";i:1;}}s:5:"value";i:1;s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:9:"addJQuery";a:7:{s:4:"name";s:9:"addJQuery";s:4:"desc";s:38:"advsearch.advsearchform_addJQuery_desc";s:4:"type";s:4:"list";s:7:"options";a:3:{i:0;a:2:{s:4:"text";s:2:"No";s:5:"value";i:0;}i:1;a:2:{s:4:"text";s:23:"Before closing HEAD tag";s:5:"value";i:1;}i:2;a:2:{s:4:"text";s:23:"Before closing BODY tag";s:5:"value";i:2;}}s:5:"value";i:1;s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:5:"addJs";a:7:{s:4:"name";s:5:"addJs";s:4:"desc";s:34:"advsearch.advsearchform_addJs_desc";s:4:"type";s:4:"list";s:7:"options";a:3:{i:0;a:2:{s:4:"text";s:2:"No";s:5:"value";i:0;}i:1;a:2:{s:4:"text";s:23:"Before closing HEAD tag";s:5:"value";i:1;}i:2;a:2:{s:4:"text";s:23:"Before closing BODY tag";s:5:"value";i:2;}}s:5:"value";i:1;s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:13:"ajaxResultsId";a:7:{s:4:"name";s:13:"ajaxResultsId";s:4:"desc";s:42:"advsearch.advsearchform_ajaxResultsId_desc";s:4:"type";s:11:"numberfield";s:7:"options";s:0:"";s:5:"value";i:0;s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:4:"asId";a:7:{s:4:"name";s:4:"asId";s:4:"desc";s:33:"advsearch.advsearchform_asId_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:3:"as0";s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:12:"clearDefault";a:7:{s:4:"name";s:12:"clearDefault";s:4:"desc";s:41:"advsearch.advsearchform_clearDefault_desc";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:2:"No";s:5:"value";i:0;}i:1;a:2:{s:4:"text";s:3:"Yes";s:5:"value";i:1;}}s:5:"value";i:1;s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:5:"debug";a:7:{s:4:"name";s:5:"debug";s:4:"desc";s:34:"advsearch.advsearchform_debug_desc";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:2:"No";s:5:"value";i:0;}i:1;a:2:{s:4:"text";s:3:"Yes";s:5:"value";i:1;}}s:5:"value";i:0;s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:4:"help";a:7:{s:4:"name";s:4:"help";s:4:"desc";s:33:"advsearch.advsearchform_help_desc";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:2:"No";s:5:"value";i:0;}i:1;a:2:{s:4:"text";s:3:"Yes";s:5:"value";i:1;}}s:5:"value";i:1;s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:12:"jsSearchForm";a:7:{s:4:"name";s:12:"jsSearchForm";s:4:"desc";s:41:"advsearch.advsearchform_jsSearchForm_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:51:"assets/components/advsearch/js/advsearchform.min.js";s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:8:"jsJQuery";a:7:{s:4:"name";s:8:"jsJQuery";s:4:"desc";s:37:"advsearch.advsearchform_jsJQuery_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:50:"assets/components/advsearch/js/jquery-1.7.1.min.js";s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:7:"landing";a:7:{s:4:"name";s:7:"landing";s:4:"desc";s:36:"advsearch.advsearchform_landing_desc";s:4:"type";s:11:"numberfield";s:7:"options";s:0:"";s:5:"value";i:0;s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:10:"liveSearch";a:7:{s:4:"name";s:10:"liveSearch";s:4:"desc";s:39:"advsearch.advsearchform_liveSearch_desc";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:2:"No";s:5:"value";i:0;}i:1;a:2:{s:4:"text";s:3:"Yes";s:5:"value";i:1;}}s:5:"value";i:0;s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:6:"method";a:7:{s:4:"name";s:6:"method";s:4:"desc";s:35:"advsearch.advsearchform_method_desc";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:4:"POST";s:5:"value";s:4:"POST";}i:1;a:2:{s:4:"text";s:3:"GET";s:5:"value";s:3:"GET";}}s:5:"value";s:3:"GET";s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:7:"opacity";a:7:{s:4:"name";s:7:"opacity";s:4:"desc";s:36:"advsearch.advsearchform_opacity_desc";s:4:"type";s:11:"numberfield";s:7:"options";s:0:"";s:5:"value";d:1;s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:11:"searchIndex";a:7:{s:4:"name";s:11:"searchIndex";s:4:"desc";s:40:"advsearch.advsearchform_searchIndex_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:6:"search";s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:13:"toPlaceholder";a:7:{s:4:"name";s:13:"toPlaceholder";s:4:"desc";s:42:"advsearch.advsearchform_toPlaceholder_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:3:"tpl";a:7:{s:4:"name";s:3:"tpl";s:4:"desc";s:32:"advsearch.advsearchform_tpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:13:"AdvSearchForm";s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:9:"urlScheme";a:7:{s:4:"name";s:9:"urlScheme";s:4:"desc";s:38:"advsearch.advsearchform_urlScheme_desc";s:4:"type";s:4:"list";s:7:"options";a:5:{i:0;a:2:{s:4:"text";s:20:"relative to site_url";s:5:"value";i:-1;}i:1;a:2:{s:4:"text";s:35:"prepended with site_url from config";s:5:"value";s:4:"full";}i:2;a:2:{s:4:"text";s:35:"prepended with base_url from config";s:5:"value";s:3:"abs";}i:3;a:2:{s:4:"text";s:35:"absolute url, forced to http scheme";s:5:"value";s:4:"http";}i:4;a:2:{s:4:"text";s:36:"absolute url, forced to https scheme";s:5:"value";s:5:"https";}}s:5:"value";i:-1;s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}s:8:"withAjax";a:7:{s:4:"name";s:8:"withAjax";s:4:"desc";s:37:"advsearch.advsearchform_withAjax_desc";s:4:"type";s:4:"list";s:7:"options";a:2:{i:0;a:2:{s:4:"text";s:13:"Non-ajax mode";s:5:"value";i:0;}i:1;a:2:{s:4:"text";s:9:"Ajax mode";s:5:"value";i:1;}}s:5:"value";i:0;s:7:"lexicon";s:20:"advsearch:properties";s:4:"area";s:0:"";}}'

-----

/**
 * AdvSearchForm
 *
 * Dynamic content search add-on that supports results highlighting and faceted searches.
 *
 * Use AdvSearchForm to display a filter & search form
 *
 * @category    Third Party Component
 * @version     1.0.0 pl
 * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 *
 * @author      Coroico <coroico@wangba.fr>
 * @date        25/01/2012
 *
 * -----------------------------------------------------------------------------
 */

require_once $modx->getOption('advsearch.core_path',null,$modx->getOption('core_path').'components/advsearch/').'model/advsearch/advsearchform.class.php';
if (!class_exists('AdvSearchForm')) {
    $this->modx->log(modX::LOG_LEVEL_ERROR,'[AdvSearch] AdvSearchForm class not found.');
    return false;
}

$asf = $modx->getOption('asId',$scriptProperties,'as0') ? $scriptProperties['asId'] : 'as0';
$asf = str_replace(' ', '',$asf) .'f';
$$asf = new AdvSearchForm($modx,$scriptProperties);

$output = $$asf->output();

return $output;