id: 8
name: QuipCount
description: 'An assistance snippet for getting thread/user comment counts.'
category: Quip
properties: 'a:5:{s:4:"type";a:7:{s:4:"name";s:4:"type";s:4:"desc";s:25:"quip.prop_count_type_desc";s:4:"type";s:4:"list";s:7:"options";a:3:{i:0;a:2:{s:4:"text";s:11:"quip.thread";s:5:"value";s:6:"thread";}i:1;a:2:{s:4:"text";s:9:"quip.user";s:5:"value";s:4:"user";}i:2;a:2:{s:4:"text";s:11:"quip.family";s:5:"value";s:6:"family";}}s:5:"value";s:6:"thread";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:6:"thread";a:7:{s:4:"name";s:6:"thread";s:4:"desc";s:27:"quip.prop_count_thread_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:4:"user";a:7:{s:4:"name";s:4:"user";s:4:"desc";s:25:"quip.prop_count_user_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:6:"family";a:7:{s:4:"name";s:6:"family";s:4:"desc";s:27:"quip.prop_count_family_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:13:"toPlaceholder";a:7:{s:4:"name";s:13:"toPlaceholder";s:4:"desc";s:34:"quip.prop_count_toplaceholder_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}}'

-----

/**
 * Quip
 *
 * Copyright 2010-11 by Shaun McCormick <shaun@modx.com>
 *
 * This file is part of Quip, a simple commenting component for MODx Revolution.
 *
 * Quip is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * Quip is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Quip; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package quip
 */
/**
 * QuipCount
 *
 * Gets the total # of comments in a thread or by a user.
 *
 * @var modX $modx
 * @var array $scriptProperties
 * @var Quip $quip
 *
 * @name QuipCount
 * @author Shaun McCormick <shaun@modx.com>
 * @package quip
 */
$quip = $modx->getService('quip','Quip',$modx->getOption('quip.core_path',null,$modx->getOption('core_path').'components/quip/').'model/quip/',$scriptProperties);
if (!($quip instanceof Quip)) return '';
$quip->initialize($modx->context->get('key'));
$controller = $quip->loadController('ThreadCount');
$output = $controller->run($scriptProperties);
return $output;