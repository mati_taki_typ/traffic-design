id: 77
source: 5
name: urlencode
properties: null
static: 1
static_file: snippets/urlencode.php

-----

$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.


//$urlen = urlencode($string);

//return $urlen;