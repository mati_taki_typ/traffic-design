id: 82
source: 1
name: atrakcje2image
description: 'Wypisanie adresu URL obrazka na podstawie nazwy atrakcji'
properties: 'a:0:{}'

-----

/*

Wypisywanie obrazka dla listy Aktrakcji, po podaniu nazwy kategorii

*/


//wyszukanie resourca, który ma templatke konfiguracjaTemplate(32)
$query = $modx->newQuery('modResource');
$query->where(Array(
    'template'=>'32',
));

// Wybranie res.
$resources = $modx->getCollection('modResource', $query);

foreach ($resources as $resource) {
        $templateVars =& $resource->getMany('TemplateVars');
        foreach ($templateVars as $tvId => $templateVar) {
            $tvs[$templateVar->get('name')] = $templateVar->get('value');
        }
    $rA = array_merge($resource->toArray(),$tvs);

}

//Przerobienie JSON na array dla tablicy TV atrakcjeListaMIGX
$arrJSON = $modx->fromJSON($rA['atrakcjeListaMIGX']);

//$szukanie='';

$i=0;
foreach($arrJSON as $arrJSONDataSet)
{
if ($arrJSON[$i]['atrakcja']==$szukanie) $output=$arrJSON[$i]['image'];
++$i;
}

echo $output;