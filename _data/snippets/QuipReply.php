id: 10
name: QuipReply
description: 'Displays a reply form for comments.'
category: Quip
properties: 'a:34:{s:11:"requireAuth";a:7:{s:4:"name";s:11:"requireAuth";s:4:"desc";s:32:"quip.prop_reply_requireauth_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:17:"requireUsergroups";a:7:{s:4:"name";s:17:"requireUsergroups";s:4:"desc";s:38:"quip.prop_reply_requireusergroups_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:14:"requirePreview";a:7:{s:4:"name";s:14:"requirePreview";s:4:"desc";s:35:"quip.prop_reply_requirepreview_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:6:"closed";a:7:{s:4:"name";s:6:"closed";s:4:"desc";s:27:"quip.prop_reply_closed_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:10:"closeAfter";a:7:{s:4:"name";s:10:"closeAfter";s:4:"desc";s:31:"quip.prop_reply_closeafter_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";i:14;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:8:"moderate";a:7:{s:4:"name";s:8:"moderate";s:4:"desc";s:29:"quip.prop_reply_moderate_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:21:"moderateAnonymousOnly";a:7:{s:4:"name";s:21:"moderateAnonymousOnly";s:4:"desc";s:42:"quip.prop_reply_moderateanonymousonly_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:21:"moderateFirstPostOnly";a:7:{s:4:"name";s:21:"moderateFirstPostOnly";s:4:"desc";s:42:"quip.prop_reply_moderatefirstpostonly_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:10:"moderators";a:7:{s:4:"name";s:10:"moderators";s:4:"desc";s:31:"quip.prop_reply_moderators_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:14:"moderatorGroup";a:7:{s:4:"name";s:14:"moderatorGroup";s:4:"desc";s:35:"quip.prop_reply_moderatorgroup_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:13:"Administrator";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:24:"dontModerateManagerUsers";a:7:{s:4:"name";s:24:"dontModerateManagerUsers";s:4:"desc";s:45:"quip.prop_reply_dontmoderatemanagerusers_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:10:"dateFormat";a:7:{s:4:"name";s:10:"dateFormat";s:4:"desc";s:31:"quip.prop_reply_dateformat_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:21:"%b %d, %Y at %I:%M %p";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:6:"useCss";a:7:{s:4:"name";s:6:"useCss";s:4:"desc";s:27:"quip.prop_reply_usecss_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:12:"notifyEmails";a:7:{s:4:"name";s:12:"notifyEmails";s:4:"desc";s:33:"quip.prop_reply_notifyemails_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:9:"recaptcha";a:7:{s:4:"name";s:9:"recaptcha";s:4:"desc";s:30:"quip.prop_reply_recaptcha_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:14:"recaptchaTheme";a:7:{s:4:"name";s:14:"recaptchaTheme";s:4:"desc";s:35:"quip.prop_reply_recaptchatheme_desc";s:4:"type";s:4:"list";s:7:"options";a:4:{i:0;a:2:{s:4:"text";s:12:"quip.opt_red";s:5:"value";s:3:"red";}i:1;a:2:{s:4:"text";s:14:"quip.opt_white";s:5:"value";s:5:"white";}i:2;a:2:{s:4:"text";s:14:"quip.opt_clean";s:5:"value";s:5:"clean";}i:3;a:2:{s:4:"text";s:19:"quip.opt_blackglass";s:5:"value";s:10:"blackglass";}}s:5:"value";s:5:"clean";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:28:"disableRecaptchaWhenLoggedIn";a:7:{s:4:"name";s:28:"disableRecaptchaWhenLoggedIn";s:4:"desc";s:49:"quip.prop_reply_disablerecaptchawhenloggedin_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:16:"autoConvertLinks";a:7:{s:4:"name";s:16:"autoConvertLinks";s:4:"desc";s:37:"quip.prop_reply_autoconvertlinks_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:24:"extraAutoLinksAttributes";a:7:{s:4:"name";s:24:"extraAutoLinksAttributes";s:4:"desc";s:45:"quip.prop_reply_extraautolinksattributes_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:11:"useGravatar";a:7:{s:4:"name";s:11:"useGravatar";s:4:"desc";s:32:"quip.prop_reply_usegravatar_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:1;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:12:"gravatarIcon";a:7:{s:4:"name";s:12:"gravatarIcon";s:4:"desc";s:33:"quip.prop_reply_gravataricon_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:9:"identicon";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:12:"gravatarSize";a:7:{s:4:"name";s:12:"gravatarSize";s:4:"desc";s:33:"quip.prop_reply_gravatarsize_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";i:50;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:13:"tplAddComment";a:7:{s:4:"name";s:13:"tplAddComment";s:4:"desc";s:34:"quip.prop_reply_tpladdcomment_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:14:"quipAddComment";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:17:"tplLoginToComment";a:7:{s:4:"name";s:17:"tplLoginToComment";s:4:"desc";s:38:"quip.prop_reply_tpllogintocomment_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:18:"quipLoginToComment";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:10:"tplPreview";a:7:{s:4:"name";s:10:"tplPreview";s:4:"desc";s:31:"quip.prop_reply_tplpreview_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:18:"quipPreviewComment";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:9:"tplReport";a:7:{s:4:"name";s:9:"tplReport";s:4:"desc";s:30:"quip.prop_reply_tplreport_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:10:"quipReport";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:10:"postAction";a:7:{s:4:"name";s:10:"postAction";s:4:"desc";s:31:"quip.prop_reply_postaction_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:9:"quip-post";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:13:"previewAction";a:7:{s:4:"name";s:13:"previewAction";s:4:"desc";s:34:"quip.prop_reply_previewaction_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:12:"quip-preview";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:8:"idPrefix";a:7:{s:4:"name";s:8:"idPrefix";s:4:"desc";s:29:"quip.prop_reply_idprefix_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:4:"qcom";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:10:"redirectTo";a:7:{s:4:"name";s:10:"redirectTo";s:4:"desc";s:31:"quip.prop_reply_redirectto_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:13:"redirectToUrl";a:7:{s:4:"name";s:13:"redirectToUrl";s:4:"desc";s:34:"quip.prop_reply_redirecttourl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:5:"debug";a:7:{s:4:"name";s:5:"debug";s:4:"desc";s:26:"quip.prop_reply_debug_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:9:"debugUser";a:7:{s:4:"name";s:9:"debugUser";s:4:"desc";s:30:"quip.prop_reply_debuguser_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}s:11:"debugUserId";a:7:{s:4:"name";s:11:"debugUserId";s:4:"desc";s:32:"quip.prop_reply_debuguserid_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:15:"quip:properties";s:4:"area";s:0:"";}}'

-----

/**
 * Quip
 *
 * Copyright 2010-11 by Shaun McCormick <shaun@modx.com>
 *
 * This file is part of Quip, a simple commenting component for MODx Revolution.
 *
 * Quip is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * Quip is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Quip; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package quip
 */
/**
 * Displays a reply form for a thread
 *
 * @var modX $modx
 * @var array $scriptProperties
 * @var Quip $quip
 *
 * @name QuipReply
 * @author Shaun McCormick <shaun@modx.com>
 * @package quip
 */
$quip = $modx->getService('quip','Quip',$modx->getOption('quip.core_path',null,$modx->getOption('core_path').'components/quip/').'model/quip/',$scriptProperties);
if (!($quip instanceof Quip)) return '';

$quip->initialize($modx->context->get('key'));
$controller = $quip->loadController('ThreadReply');
$output = $controller->run($scriptProperties);
return $output;

/* get thread */
$thread = $modx->getOption('quip_thread',$_REQUEST,$modx->getOption('thread',$scriptProperties,''));
if (empty($thread)) return '';
$thread = $modx->getObject('quipThread',array('name' => $thread));
if (!$thread) return '';

/* sync properties with thread row values */
$thread->sync($scriptProperties);
$ps = $thread->get('quipreply_call_params');
if (!empty($ps)) {
    $diff = array_diff_assoc($ps,$scriptProperties);
    if (empty($diff)) $diff = array_diff_assoc($scriptProperties,$ps);
}
if (empty($_REQUEST['quip_thread']) && (!empty($diff) || empty($ps))) { /* only sync call params if not on threaded reply page */
    $thread->set('quipreply_call_params',$scriptProperties);
    $thread->save();
}
/* if in threaded reply page, get the original passing values to QuipReply in the thread's main page and use those */
if (!empty($_REQUEST['quip_thread']) && is_array($ps) && !empty($ps)) $scriptProperties = array_merge($scriptProperties,$ps);
unset($ps,$diff);

/* setup default properties */
$requireAuth = $modx->getOption('requireAuth',$scriptProperties,false);
$requireUsergroups = $modx->getOption('requireUsergroups',$scriptProperties,false);
$addCommentTpl = $modx->getOption('tplAddComment',$scriptProperties,'quipAddComment');
$loginToCommentTpl = $modx->getOption('tplLoginToComment',$scriptProperties,'quipLoginToComment');
$previewTpl = $modx->getOption('tplPreview',$scriptProperties,'quipPreviewComment');
$closeAfter = $modx->getOption('closeAfter',$scriptProperties,14);
$requirePreview = $modx->getOption('requirePreview',$scriptProperties,false);
$previewAction = $modx->getOption('previewAction',$scriptProperties,'quip-preview');
$postAction = $modx->getOption('postAction',$scriptProperties,'quip-post');
$allowedTags = $modx->getOption('quip.allowed_tags',$scriptProperties,'<br><b><i>');
$preHooks = $modx->getOption('preHooks',$scriptProperties,'');
$postHooks = $modx->getOption('postHooks',$scriptProperties,'');
$unsubscribeAction = $modx->getOption('unsubscribeAction',$scriptProperties,'quip_unsubscribe');

/* get parent and auth */
$parent = $modx->getOption('quip_parent',$_REQUEST,$modx->getOption('parent',$scriptProperties,0));
$hasAuth = $modx->user->hasSessionContext($modx->context->get('key')) || $modx->getOption('debug',$scriptProperties,false) || empty($requireAuth);
if (!empty($requireUsergroups)) {
    $requireUsergroups = explode(',',$requireUsergroups);
    $hasAuth = $modx->user->isMember($requireUsergroups);
}

/* setup default placeholders */
$placeholders = array();
$p = $modx->request->getParameters();
unset($p['reported'],$p['quip_approved']);
$placeholders['parent'] = $parent;
$placeholders['thread'] = $thread->get('name');
$placeholders['url'] = $modx->makeUrl($modx->resource->get('id'),'',$p);
$placeholders['idprefix'] = $thread->get('idprefix');

/* handle POST */
$fields = array();
$hasPreview = false;
if (!empty($_POST)) {
    foreach ($_POST as $k => $v) {
        $fields[$k] = str_replace(array('[',']'),array('&#91;','&#93;'),$v);
    }
    
    $fields['name'] = strip_tags($fields['name']);
    $fields['email'] = strip_tags($fields['email']);
    $fields['website'] = strip_tags($fields['website']);
    
    /* verify a message was posted */
    if (empty($fields['comment'])) $errors['comment'] = $modx->lexicon('quip.message_err_ns');
    if (empty($fields['name'])) $errors['name'] = $modx->lexicon('quip.name_err_ns');
    if (empty($fields['email'])) $errors['email'] = $modx->lexicon('quip.email_err_ns');
    
    if (!empty($_POST[$postAction]) && empty($errors)) {
        $comment = include_once $quip->config['processorsPath'].'web/comment/create.php';
        if (is_object($comment) && $comment instanceof quipComment) {
            $params = $modx->request->getParameters();
            unset($params[$postAction],$params['quip_parent'],$params['quip_thread']);
            $params['quip_approved'] = $comment->get('approved') ? 1 : 0;
            $url = $comment->makeUrl('',$params);

            /* if not approved, remove # and replace with success message #
             * since comment is not yet visible
             */
            if (!$comment->get('approved')) {
                $url = str_replace('#'.$thread->get('idprefix').$comment->get('id'),'#quip-success-'.$thread->get('idprefix'),$url);
            }
            $modx->sendRedirect($url);
        } else if (is_array($comment)) {
            $errors = array_merge($errors,$comment);
        }
        $fields[$previewAction] = true;
    }
    /* handle preview */
    else if (!empty($fields[$previewAction]) && empty($errors)) {
        $errors = include_once $quip->config['processorsPath'].'web/comment/preview.php';
    }
    if (!empty($errors)) {
        $placeholders['error'] = implode("<br />\n",$errors);
        foreach ($errors as $k => $v) {
            $placeholders['error.'.$k] = $v;
        }
        $placeholders = array_merge($placeholders,$fields);
    }
}
/* display moderated success message */
if (isset($_GET['quip_approved']) && $_GET['quip_approved'] == 0) {
    $placeholders['successMsg'] = $modx->lexicon('quip.comment_will_be_moderated');
}

/* handle unsubscribing from thread */
if (!empty($_GET[$unsubscribeAction]) && $modx->user->hasSessionContext($modx->context->get('key'))) {
    $profile = $modx->user->getOne('Profile');
    if ($profile) {
        $notify = $modx->getObject('quipCommentNotify',array(
            'email' => $profile->get('email'),
            'thread' => $thread,
        ));
        if ($notify && $notify->remove()) {
            $placeholders['successMsg'] = $modx->lexicon('quip.unsubscribed');
        }
    }
}

/* if using recaptcha, load recaptcha html if user is not logged in */
$disableRecaptchaWhenLoggedIn = (boolean)$modx->getOption('disableRecaptchaWhenLoggedIn',$scriptProperties,true);
$useRecaptcha = (boolean)$modx->getOption('recaptcha',$scriptProperties,false);
if ($useRecaptcha && !($disableRecaptchaWhenLoggedIn && $hasAuth) && !$hasPreview) {
    $recaptcha = $modx->getService('recaptcha','reCaptcha',$quip->config['modelPath'].'recaptcha/');
    if ($recaptcha instanceof reCaptcha) {
        $recaptchaTheme = $modx->getOption('recaptchaTheme',$scriptProperties,'clean');
        $html = $recaptcha->getHtml($recaptchaTheme);
        $modx->setPlaceholder('quip.recaptcha_html',$html);
    } else {
        return $modx->lexicon('quip.recaptcha_err_load');
    }
}

/* build reply form */
$replyForm = '';

$stillOpen = $thread->checkIfStillOpen($closeAfter) && !$modx->getOption('closed',$scriptProperties,false);
if ($hasAuth && $stillOpen) {
    $phs = array_merge($placeholders,array(
        'username' => $modx->user->get('username'),
    ));
    $phs['unsubscribe'] = '';

    /* prefill fields */
    $profile = $modx->user->getOne('Profile');
    if ($profile) {
        $phs['name'] = !empty($fields['name']) ? $fields['name'] : $profile->get('fullname');
        $phs['email'] = !empty($fields['email']) ? $fields['email'] : $profile->get('email');
        $phs['website'] = !empty($fields['website']) ? $fields['website'] : $profile->get('website');

        /* allow for unsubscribing for logged-in users */
        if ($modx->user->hasSessionContext($modx->context->get('key'))) {
            $notify = $modx->getObject('quipCommentNotify',array(
                'email' => $profile->get('email'),
                'thread' => $thread,
            ));
            if ($notify) {
                $phs['notifyId'] = $notify->get('id');
                $phs['unsubscribe'] = $quip->getChunk('quipUnsubscribe',$phs);
                $params = $modx->request->getParameters();
                $params[$unsubscribeAction] = 1;
                $phs['unsubscribeUrl'] = $modx->makeUrl($modx->resource->get('id'),'',$params);
            }
        }
    }

    /* if requirePreview == false, auto-can post */
    if (!$requirePreview) {
        $phs['can_post'] = true;
    }
    $phs['post_action'] = $postAction;
    $phs['preview_action'] = $previewAction;
    $phs['allowed_tags'] = $allowedTags;
    $phs['notifyChecked'] = !empty($fields['notify']) ? ' checked="checked"' : '';

    $replyForm = $quip->getChunk($addCommentTpl,$phs);
} else if (!$stillOpen) {
    $replyForm = $modx->lexicon('quip.thread_autoclosed');
} else {
    $replyForm = $quip->getChunk($loginToCommentTpl,$placeholders);
}

/* output or set to placeholder */
$toPlaceholder = $modx->getOption('toPlaceholder',$scriptProperties,false);
if ($toPlaceholder) {
    $modx->setPlaceholder($toPlaceholder,$replyForm);
    return '';
}
return $replyForm;