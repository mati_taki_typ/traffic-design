id: 60
name: PollsLatest
description: 'Shows the latest poll of all categories or from a certain category'
category: Polls
properties: 'a:9:{s:9:"answerVar";a:7:{s:4:"name";s:9:"answerVar";s:4:"desc";s:96:"The name of the answer post variable. This could not be empty and could be default for all polls";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:6:"answer";s:7:"lexicon";N;s:4:"area";s:0:"";}s:8:"category";a:7:{s:4:"name";s:8:"category";s:4:"desc";s:77:"(Optional) When set: will select the latest poll from the given category (id)";s:4:"type";s:7:"integer";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"resultLinkVar";a:7:{s:4:"name";s:13:"resultLinkVar";s:4:"desc";s:90:"(Optional) when using resultResource, this is the paramatername the snippet is looking for";s:4:"type";s:7:"integer";s:7:"options";a:0:{}s:5:"value";s:4:"poll";s:7:"lexicon";N;s:4:"area";s:0:"";}s:14:"resultResource";a:7:{s:4:"name";s:14:"resultResource";s:4:"desc";s:89:"(Optional) when set to a resource id, this resource will be used to show the poll results";s:4:"type";s:7:"integer";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"submitVar";a:7:{s:4:"name";s:9:"submitVar";s:4:"desc";s:167:"A name wich is provided when posting a vote. This is needed to use multiple polls on one page. This could be the name of the submitbutton or a hidden field in the form";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:8:"pollVote";s:7:"lexicon";N;s:4:"area";s:0:"";}s:9:"tplResult";a:7:{s:4:"name";s:9:"tplResult";s:4:"desc";s:49:"The main result template for the latest poll view";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:22:"pollsLatestResultOuter";s:7:"lexicon";N;s:4:"area";s:0:"";}s:15:"tplResultAnswer";a:7:{s:4:"name";s:15:"tplResultAnswer";s:4:"desc";s:57:"The result template for the answers inside the outer view";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:23:"pollsLatestResultAnswer";s:7:"lexicon";N;s:4:"area";s:0:"";}s:7:"tplVote";a:7:{s:4:"name";s:7:"tplVote";s:4:"desc";s:47:"The main form template for the latest poll view";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:20:"pollsLatestVoteOuter";s:7:"lexicon";N;s:4:"area";s:0:"";}s:13:"tplVoteAnswer";a:7:{s:4:"name";s:13:"tplVoteAnswer";s:4:"desc";s:54:"The form template for the answers inside the main view";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:21:"pollsLatestVoteAnswer";s:7:"lexicon";N;s:4:"area";s:0:"";}}'

-----

/**
 * PollsLatest
 * Shows the latest poll of all categories or from a certain category
 *
 * @author Bert Oost @ OostDesign
 * @copyright Copyright Bert Oost <bert@oostdesign.nl>
 * @package polls
 *
 * TEMPLATES:
 *
 * tplVote - The main form template for the latest poll view
 * tplVoteAnswer - The form template for the answers inside the main view
 * tplResult - The main result template for the latest poll view
 * tplResultAnswer - The result template for the answers inside the outer view
 *
 * SELECTION:
 *
 * category - (Opt) will select the latest poll from the given category (id or name), could be multiple devided by a comma
 * sortby - (Opt) to influence the normal order, order could be any field in list, defaults to id
 * sortdir - (Opt) to influence the normal order direction, defaults to DESC
 * [Note] No params; will select the latest poll from any category
 *        sortby and sortdir are normally not need to set
 *
 * LINKING:
 *
 * resultResource - (Opt) when set to a resource id, this resource will be used to show the poll results
 * resultLinkVar - (Opt) when using resultResource, this is the paramatername the snippet is looking for
 */
$polls = $modx->getService('polls','Polls',$modx->getOption('polls.core_path',null,$modx->getOption('core_path').'components/polls/').'model/polls/',$scriptProperties);
if (!($polls instanceof Polls)) return '';

// templates
$tplVote = $modx->getOption('tplVote', $scriptProperties, 'pollsLatestVoteOuter');
$tplVoteAnswer = $modx->getOption('tplVoteAnswer', $scriptProperties, 'pollsLatestVoteAnswer');
$tplResult = $modx->getOption('tplResult', $scriptProperties, 'pollsLatestResultOuter');
$tplResultAnswer = $modx->getOption('tplResultAnswer', $scriptProperties, 'pollsLatestResultAnswer');

// properties
$category = $modx->getOption('category', $scriptProperties, null);
$sortby = $modx->getOption('sortby', $scriptProperties, 'id');
$sortdir = $modx->getOption('sortdir', $scriptProperties, 'DESC');
$resultResource = $modx->getOption('resultResource', $scriptProperties, null);
$resultLinkVar = $modx->getOption('resultLinkVar', $scriptProperties, 'poll');

// start getting latest poll
$c = $modx->newQuery('modPollQuestion');

if(!empty($category)) {
  $categories = explode(',',$category);
  foreach($categories as $categoryid) {
    $c->orCondition(array(
      'category:=' => $categoryid
    ));
  }
}

$c->where(array(
  "(`modPollQuestion`.`publishdate` <= '".date('Y-m-d H:i:s')."' OR `modPollQuestion`.`publishdate` IS NULL)",
  "(`modPollQuestion`.`unpublishdate` >= '".date('Y-m-d H:i:s')."' OR `modPollQuestion`.`unpublishdate` IS NULL)"
));
$c->andCondition(array('modPollQuestion.hide:=' => '0'));
$c->sortby($sortby, $sortdir);
$c->limit(1);

$latest = $modx->getObject('modPollQuestion', $c);

if(!empty($latest)) {
  
  // start voting (if submitted)
  if($latest->vote($scriptProperties)) {
    
    $url = $modx->makeUrl($modx->resource->get('id'));
    $modx->sendRedirect($url);
  }
  
  $modx->lexicon->load('polls:latestpoll');
  
  $placeholders = $latest->toArray();
  $placeholders['totalVotes'] = $latest->getTotalVotes();

  $category = $latest->getOne('Category');
  $placeholders['category_name'] = (!empty($category) && is_object($category)) ? $category->get('name') : '';
  
  $answers = $latest->getMany('Answers');
  $answersOutput = '';
  foreach($answers as $idx => $answer) {
    $answerParams = array_merge(
      $answer->toArray(), array(
	'percent' => $answer->getVotesPercent($placeholders['totalVotes']),
	'idx' => $idx
      )
    );
    $answersOutput .= $modx->getChunk((!$latest->hasVoted() ? $tplVoteAnswer : $tplResultAnswer), $answerParams);
  }
  
  $placeholders['answers'] = $answersOutput;
  
  if($latest->hasVoted()) {
    
    $vote = $latest->getOne('Logs', array('ipaddress:=' => $_SERVER['REMOTE_ADDR']));
    $placeholders['logdate'] = $vote->get('logdate');
  }
  
  // build resource url for results if not has voted, because then the results are showed
  if(!empty($resultResource) && is_numeric($resultResource) && $resultResource > 0 && !$latest->hasVoted()) {
    
    $resource = $modx->getObject('modResource', $resultResource);
    
    if(!empty($resource)) {
      $url = $modx->makeUrl($resource->get('id'), '', '&'.$resultLinkVar.'='.$latest->get('id'));
      $placeholders['results_url'] = $url;
    }
  }
  
  $output = $modx->getChunk((!$latest->hasVoted() ? $tplVote : $tplResult), $placeholders);
  
  return $output;
}

return '';