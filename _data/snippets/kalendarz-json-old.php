id: 125
source: 1
name: kalendarz_json_old
description: 'Tworzenie opcji do TV kalendarz_opcje listbox z JSON'
category: Kalendarz
properties: 'a:0:{}'

-----

/*
* ps. wczytanie jsona i przerobienie go na opcje jako $input podajemy url
*/

$output = '';
$input = '';
$ctx = $modx->getContext('web');
if ($ctx) {

	$input = $ctx->getOption('json_kalendarz', null, 'default'); 

} else {
     return '';
}

$array = $modx->fromJSON(file_get_contents($input, 1000000));

foreach ($array as $k) {
    $output.=$k['nazwa'].'=='.$k['lp'].'||';

}

//cut last ||
return substr($output,0,strlen($output)-2);