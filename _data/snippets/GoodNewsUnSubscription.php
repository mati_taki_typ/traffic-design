id: 117
name: GoodNewsUnSubscription
description: 'Handles one-click unsubscription in the front-end.'
category: GoodNews
properties: 'a:8:{s:6:"errTpl";a:7:{s:4:"name";s:6:"errTpl";s:4:"desc";s:39:"prop_goodnewsunsubscription.errtpl_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:37:"<span class="error">[[+error]]</span>";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:8:"preHooks";a:7:{s:4:"name";s:8:"preHooks";s:4:"desc";s:41:"prop_goodnewsunsubscription.prehooks_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:9:"postHooks";a:7:{s:4:"name";s:9:"postHooks";s:4:"desc";s:42:"prop_goodnewsunsubscription.posthooks_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:20:"sendUnauthorizedPage";a:7:{s:4:"name";s:20:"sendUnauthorizedPage";s:4:"desc";s:53:"prop_goodnewsunsubscription.sendunauthorizedpage_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:9:"submitVar";a:7:{s:4:"name";s:9:"submitVar";s:4:"desc";s:42:"prop_goodnewsunsubscription.submitvar_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:24:"goodnews-unsubscribe-btn";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:10:"successKey";a:7:{s:4:"name";s:10:"successKey";s:4:"desc";s:43:"prop_goodnewsunsubscription.successkey_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:10:"updsuccess";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:14:"removeUserData";a:7:{s:4:"name";s:14:"removeUserData";s:4:"desc";s:47:"prop_goodnewsunsubscription.removeuserdata_desc";s:4:"type";s:13:"combo-boolean";s:7:"options";s:0:"";s:5:"value";b:0;s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}s:17:"placeholderPrefix";a:7:{s:4:"name";s:17:"placeholderPrefix";s:4:"desc";s:50:"prop_goodnewsunsubscription.placeholderprefix_desc";s:4:"type";s:9:"textfield";s:7:"options";s:0:"";s:5:"value";s:0:"";s:7:"lexicon";s:19:"goodnews:properties";s:4:"area";s:0:"";}}'

-----

/**
 * GoodNews
 *
 * Copyright 2012 by bitego <office@bitego.com>
 *
 * GoodNews is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * GoodNews is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this software; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */

/**
 * Snippet which handles one-click unsubscription through email link.
 *
 * @var modX $modx
 * @var GoodNewsSubscription $goodnewssubscription
 * @var array $scriptProperties
 *
 * @property string $errTpl Chunk for error output (default: <span class="error">[[+error]]</span>)
 * @property string $preHooks A comma-separated list of 'hooks' (snippets), that will be executed before the user is unsubscribed but after validation.
 * @property string $postHooks A comma-separated list of 'hooks' (snippets), that will be executed after the user is unsubscribed.
 * @property boolean $sendUnauthorizedPage Whether or not to redirect a subscriber to the unauthorized page if his authentication is not verified (default: 0 = false)
 * @property string $submitVar The name of the form submit button that triggers the submission. (default: goodnews-unsubscribe-btn)
 * @property string $successKey The name of the key submitted as url param in case of success (default: unsubsuccess)
 * @property boolean $removeUserData Whether or not to completely remove all user data from database after unsubscription (default: 0 = false)
 * @property string $placeholderPrefix The prefix to use for all placeholders set by this snippet. (default: '')
 *
 * @package goodnews
 */

require_once $modx->getOption('goodnews.core_path', null, $modx->getOption('core_path').'components/goodnews/').'model/goodnews/goodnewssubscription.class.php';
$goodnewssubscription = new GoodNewsSubscription($modx, $scriptProperties);

$controller = $goodnewssubscription->loadController('UnSubscription');
$output = $controller->run($scriptProperties);
return $output;