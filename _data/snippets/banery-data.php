id: 108
name: banery_data
description: 'Wyświetla baner między 2 datami'
category: BannerY
properties: null

-----

//jesli nie ma ustawionych zadnych dat
$info='0';

if (isset($y)) {
//dla ustawionych obu dat, poczatku i konca
if((time() > strtotime($x)) && (time() < strtotime($y))) $info='1';

}
//dla ustawionej daty tylko poczatku
else if(time() > strtotime($x)) $info='1';

return $info;