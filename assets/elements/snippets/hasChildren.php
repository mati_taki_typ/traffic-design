<?php
$docId = isset($id) ? intval($id) : $modx->resource->get('id');


if($docId==0){
	return 1; //dla resourców bez parenta
}
else{

		
		$document = $modx->getObject('modResource', $docId);
		
		 $tv_menu = $document->getTVValue($tv);
		
		 $hasChildren = $document->hasChildren();
		 
		if($tv_menu==1 &&  $hasChildren>0){ 
			//jeśli ma dzieci i menu_gorne zaznaczone
			return 1;
	
			
		}
	
		elseif(empty($tv_menu) &&  $hasChildren>0){     
		
			//jeśli ma dzieci i menu_gorne niezaznaczone
		  	return 2;
		
			
		}
		
		//jeśli nie ma dzieci
		else{
			
			 return 0;
			}
}